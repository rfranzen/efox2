<?php

	require "config/config.php";
	require "classes/efox.class.php";
	require "classes/autentica.php";
	require "classes/formulario.class.php";
	require "classes/template.class.php";

	//Cria um novo objeto da classe Tema que eh usada pelos modulos
	//(precisa ser iniciada antes de se chamar os modulos)
	$template = new Tema;
	$template->clear_all_cache();

	//Parametros de URL
	$sessao = $_GET["sessao"];
	$menu = $_GET["menu"];
	$op = $_GET["op"];

// Caso o login seja feito
	if(isset($_SESSION['codigo'])) {
		include "modulos/mod_menu.php";

		//Conteudo central
		if($menu != NULL) {
			$url = "modulos/mod_" . $menu . ".php";
			include $url;
		}else {
			include "modulos/principal.php";
		}

		$template->display('layout.php');
	}
// Caso nao efetue login
	else {
		include "modulos/mod_login.php";
		$template->display('login.php');
	}

?>