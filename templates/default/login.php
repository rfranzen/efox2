<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-BR" xml:lang="pt-BR">
<html>
<head>
  <title>{$app_name} - {$titulo}</title>
  <meta name="title" content="{$titulo} - {$app_name}" />
  <meta name="description" content="Sistema de gerenciamento para Grupos Escoteiros" />
  <meta name="author" content="Ricardo Franzen" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" href="templates/default/css/login.css" type="text/css" media="screen" />
</head>

<body>
	<img src="templates/default/imagens/logo.png" alt="Logo" class="logo" />
	<div class="barraTop">{$app_name}</div>
	<div id="login">
		<img src="templates/default/imagens/logo-small.png" alt="Logo {$app_name}" />
		<p>Grupo Escoteiro Georg Edward Fox - 027</p>
		{$login}
	</div>
	<div class="barraFim">
		<p>
			&copy;2008 Grupo Escoteiro Georg Edward Fox - 027 - RS<br />
		</p>
	</div>
</body>
</html>
