<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-BR" xml:lang="pt-BR">
<html>
<head>
	<title>{$app_name} - {$titulo}</title>
	<meta name="title" content="{$titulo} - {$app_name}" />
	<meta name="description" content="Sistema de gerenciamento para Grupos Escoteiros" />
	<meta name="author" content="Ricardo Franzen" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="/efox2/templates/default/css/estilo.css" type="text/css" media="screen" />
</head>

<body>
	<div id="corpo">{$corpo}</div>
</body>
</html>
