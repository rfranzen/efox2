<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-BR" xml:lang="pt-BR">
<html>
<head>
	<title>{$titulo} :: {$app_name}</title>
	<meta name="title" content="{$titulo} - {$app_name}" />
	<meta name="description" content="Sistema de gerenciamento para Grupos Escoteiros" />
	<meta name="author" content="Ricardo Franzen" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="templates/default/css/estilo.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="templates/default/css/impressao.css" type="text/css" media="print" />
</head>

<body>
	<div id="menuTopo">
		{$menuTopo}
		<div class="logout">{$menuLogof}</div>
	</div>
	<spam id="logo"><a href="index.php"><img src="templates/default/imagens/logo.png" alt="eFox2 Logo" title="eFox2" /></a></spam>
	<div id="menuDir">{$menuDir}</div>
	<div id="corpo">{$corpo}</div>
	<div id="rodape">
		<p>
			&copy;2008 Grupo Escoteiro Georg Edward Fox - 027 - RS<br />
		</p>
	</div>
</body>
</html>
