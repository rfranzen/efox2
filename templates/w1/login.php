<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-BR" xml:lang="pt-BR">
<html>
<head>
	<title>{$titulo} :: {$app_name}</title>
	<meta name="title" content="{$titulo} - {$app_name}" />
	<meta name="description" content="Sistema de gerenciamento para Grupos Escoteiros" />
	<meta name="author" content="Ricardo Franzen" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="templates/w1/login.css" type="text/css" media="screen" />
</head>

<body>
<div id="site">
	<div id="logo">
    	<a href="index.php" title="Home" target="_self"><img src="templates/w1/images/logo.jpg" width="258" height="82" border="0" alt="eFOX" title="eFOX"></a>
		</div>
	<div id="login">
    	{$login}
    </div>
	<div id="publicidade">
    	<a href="index.php" title="Home" target="_self"><img src="templates/w1/images/publicidade.jpg" width="585" height="74" border="0" alt="eFOX" title="Publicidade"></a>
    </div>
    <div id="rodape">
		<p>
			&copy;2008 Grupo Escoteiro Georg Edward Fox - 027 - RS<br />
		</p>
    </div>
</div>
</body>
</html>
