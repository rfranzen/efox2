/*
 * Tabelas e padroes - efox2.0
*/

/* Tabela usuario */
CREATE TABLE usuario (
    codigo INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    ueb INTEGER(10),
    ativo BOOL DEFAULT '1',
    login VARCHAR(30) NULL,
    email VARCHAR(50) NULL,
    nivel INTEGER(1) UNSIGNED NULL DEFAULT '3',
    senha VARCHAR(100) NULL,
    nome VARCHAR(100) NULL,
    dataueb INTEGER(4) UNSIGNED NULL,
    url_foto VARCHAR(100) NULL,
    nascimento DATE NULL,
    cidade INTEGER(4) UNSIGNED NULL,
    endereco VARCHAR(50) NULL,
    numero INTEGER(4) UNSIGNED NULL,
    bairro VARCHAR(50) NULL,
    fone INTEGER(8) UNSIGNED NULL,
    naturalidade VARCHAR(20) NULL,
    estado_civil VARCHAR(10) NULL,
    religiao VARCHAR(20) NULL,
    serie VARCHAR(4) NULL,
    turno VARCHAR(5) NULL,
    instituicao VARCHAR(50) NULL,
    nome_pai VARCHAR(50) NULL,
    profissao_pai VARCHAR(30) NULL,
    fone_pai INTEGER(8) UNSIGNED NULL,
    nome_mae VARCHAR(50) NULL,
    profissao_mae VARCHAR(30) NULL,
    fone_mae INTEGER(8) UNSIGNED NULL,
    PRIMARY KEY(codigo)
);

INSERT INTO usuario(ueb,login,email,nivel,senha,nome,dataueb) VALUES ('123456','ricardo','rfranzen@gmail.com','1','emFfDLTMgco..','Ricardo Franzen','1998');

/* Tabela objeto */
CREATE TABLE objeto (
	codigo INTEGER(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	categoria VARCHAR(20) NULL,
	nome VARCHAR(100) NULL,
	tipo VARCHAR(50) NULL,
	rLobo BOOL NULL DEFAULT '0',
	rEscot BOOL NULL DEFAULT '0',
	rSen BOOL NULL DEFAULT '0',
	rPio BOOL NULL DEFAULT '0',
	area VARCHAR(50) NULL,
	cod_responsavel INTEGER(10) UNSIGNED NULL,
	cod_assistente VARCHAR(30) NULL,
	dataini DATE NULL,
	datafim DATE NULL,
	total INTEGER(4) UNSIGNED NULL,
	descricao TEXT NULL,
	PRIMARY KEY(codigo)
);

INSERT INTO objeto(categoria,nome) VALUES ('ramo','Lobo'),('ramo','Escoteiro'),('ramo','Senior'),('ramo','Pioneiro'),('ramo','Chefia'),('ramo','Diretoria'),('especialidade','Selecione a Especialidade'),('especialidade_tipo','Selecione'),('especialidade_tipo','Ciencia e Tecnologia'),('especialidade_tipo','Cultura'),('especialidade_tipo','Desportos'),('especialidade_tipo','Especial (Cordoes,Lis,etc'),('especialidade_tipo','Habilidades Escoteiras'),('especialidade_tipo','Servicos');

/* Tabela menu */
CREATE TABLE menu (
    codigo INTEGER(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    nivel INTEGER(1) NULL,
	titulo VARCHAR(100) NULL,
    pai INTEGER(3) NULL,
    nome VARCHAR(50) NULL,
    posicao INTEGER(1) NULL,
    classe VARCHAR(20) NULL,
    args VARCHAR(30) NULL,
    PRIMARY KEY(codigo)
);

INSERT INTO menu(nivel,titulo,pai,nome,posicao,classe,args) VALUES
	('1',NULL,NULL,'Administra&ccedil;&atilde;o','1',NULL,'adm'),
	('1',NULL,NULL,'Cadastro','1',NULL,'cadastro'),
	('1',NULL,NULL,'Relat&oacute;rios','1',NULL,'relatorios'),

	('2',NULL,NULL,'Meus Dados','1',NULL,'usrinf'),
	('2',NULL,NULL,'Cadastro','1',NULL,'cadastro'),
	('2',NULL,NULL,'Relat&oacute;rios','1',NULL,'relatorios'),

	('3',NULL,NULL,'Meus Dados','1',NULL,'usrinf'),
	('3',NULL,NULL,'Meu Hist&oacute;rico','1',NULL,'usrhist'),

	('1','Op&ccedil;&otilde;es do Sistema','1','Op&ccedil;&otilde;es','2',NULL,'adm_opcoes'),
	('1','Gerenciamento de Usu&aacute;rios','1','Usu&aacute;rios','2',NULL,'adm_usuario'),
	('1','Gerenciamento de Menus','1','Menus','2',NULL,'adm_menu'),
	('1','Cadastro de Atividade','2','Atividade','2',NULL,'atividade'),
	('1','Usu&aacute;rios por Atividade','2','Atividade / Usu&aacute;rios','2',NULL,'usr-atividade'),
	('1','Cadastro de Especialidade','2','Especialidade','2',NULL,'especialidade'),
	('1','Usu&aacute;rios por Especialidade','2','Especialidade / Usu&aacute;rios','2',NULL,'usr-especialidade'),
	('1','Cadastro de Pessoas','2','Pessoa','2',NULL,'pessoa'),
	('1','Cadastro de Cidades','2','Cidade','2',NULL,'cidade'),
	('1','Cadastro de Grupos Escoteiros','2','Grupo Escoteiro','2',NULL,'grupo'),
	('1','Cadastro de Ramo','2','Ramo','2',NULL,'ramo'),
	('1','Relat&oacute;rios dos Usu&aacute;rios','3','Listar Usuarios','2',NULL,'lista'),
	('1','Fichas e Formul&aacute;rios','3','Formul&aacute;rios','2',NULL,'rel_formularios'),
	('1','Fichas de Atividade','3','Atividades','2',NULL,'lista'),

	('2','Dados do Usu&aacute;rio','4','Informa&ccedil;&otilde;es','2',NULL,'usr_info'),
	('2','Alterar a Senha','4','Alterar Senha','2',NULL,'usr_senha'),
	('2','Cadastro de Atividade','5','Atividade','2',NULL,'atividade'),
	('2','Usu&aacute;rios por Atividade','5','Atividade / Usu&aacute;rios','2',NULL,'usr-atividade'),
	('2','Cadastro de Especialidade','5','Especialidade','2',NULL,'especialidade'),
	('2','Usu&aacute;rios por Especialidade','5','Especialidade / Usu&aacute;rios','2',NULL,'usr-especialidade'),
	('2','Cadastro de Pessoas','5','Pessoa','2',NULL,'pessoa'),
	('2','Cadastro de Ramo','5','Ramo','2',NULL,'ramo'),
	('2','Relat&oacute;rios dos Usu&aacute;rios','6','Listar Usuarios','2',NULL,'rel_lista'),
	('2','Fichas e Formul&aacute;rios','6','Formul&aacute;rios','2',NULL,'rel_formularios'),
	('2','Fichas de Atividade','6','Atividades','2',NULL,'rel_atividade'),

	('3','Dados do Usu&aacute;rio','7','Informa&ccedil;&otilde;es','2',NULL,'usr_info'),
	('3','Alterar a Senha','7','Alterar Senha','2',NULL,'usr_senha'),
	('3','Minha Ficha 120','8','Ficha 120','2',NULL,'usr_120'),
	('3','Minhas Especialidades','8','Especialidades','2',NULL,'usr_especialidade');


/* Tabela de relacionamento usuario - objeto */
CREATE TABLE usuario_objeto (
	codigo_usuario INTEGER(10) UNSIGNED NOT NULL,
	codigo_objeto INTEGER(4) UNSIGNED NOT NULL,
	nivel INTEGER(1) UNSIGNED NULL,
    itens1 VARCHAR(20) NULL,
    data1 DATE NULL,
    itens2 VARCHAR(20) NULL,
    data2 DATE NULL,
    itens3 VARCHAR(20) NULL,
    data3 DATE NULL,
    grupo INTEGER(4) UNSIGNED NULL,
    inscricao DATE NULL,
    desligamento DATE NULL,
	PRIMARY KEY(codigo_usuario, codigo_objeto),
    INDEX usuario_has_atividade_FKIndex1(codigo_usuario),
    INDEX usuario_has_atividade_FKIndex2(codigo_objeto)
);
