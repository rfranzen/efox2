<?php
class Bd {

  public $bd;
  public $id;
  private $db_host = SERVIDOR;
  private $banco = BANCO;
  private $usuario = USUARIO;
  private $senha = SENHA;
  private $porta = "3306";

  function conectar($bd,$db_host,$usuario,$senha) {
    $this->bd = $bd;
    if ($this->bd=="postgresql") {
      $cstr = "dbname=".$bd." host=".$db_host." port=".$porta." user=".$usuario." password=".$senha;
      $this->id = pg_connect($cstr);
    }
    else {
      $this->id = mysql_connect($db_host,$usuario,$senha);
      if ($this->id) {
        mysql_select_db($bd,$this->id);
      }
      else
        $this->id = 0;
    }
  }
}// fim da classe bd

class Consulta {
  public $bd;
  public $res;
  public $row;
  public $nrw;
  public $data;

  function consulta(&$bd) {
    $this->bd = $bd;
  }

  function executa($sql="",$tipo="") {
    if ($sql=="") {
      $this->res = 0;
      $this->nrw = 0;
      $this->row = -1;
    }
    if($this->bd->bd=="postgresql") {
      $this->res = pg_exec($this->bd->id,$sql);
      $this->nrw = pg_NumRows($this->res);
    }
    else {
      //echo "Debuging: ".$sql."<br />";
      $this->res = mysql_query($sql,$this->bd->id);
      $this->nrw = mysql_num_rows($this->res);
    }
    $this->row = 0;
    if ($this->nrw>0) {
      $this->dados();
    }
  }

  function primeiro() {
    $this->row = 0;
    $this->dados();
  }

  function proximo() {
    $this->row = ($this->row<($this->nrw-1))?++$this->row:($this->nrw - 1);
    $this->dados();
  }

  function anterior() {
    $this->row = ($this->row>0) ? --$this->row : 0;
    $this->dados();
  }

  function ultimo() {
    $this->row = $this->nrw-1;
    $this->dados();
  }

  function navega($linha) {
    if ($linha>=0 AND $linha<$this->nrw) {
      $this->row = $linha;
      $this->dados();
    }
  }

  function dados() {
    if ($this->bd->bd=="postgresql")
      $this->data = pg_fetch_array($this->res,$this->row);
    else {
      mysql_data_seek($this->res,$this->row);
      $this->data = mysql_fetch_array($this->res);
    }
  }

  function last_id($seq="",$sql="SELECT LAST_INSERT_ID()") {
    if ($this->bd=="postgresql") {
      $sql = "SELECT CURRVAL('$seq')";
      $this->executa($sql);
      if (!$this->res)
        return 0;
        return $this->data[0];
    }
    else {
      $this->executa($sql);
      return $this->data[0];
    }
  }
}//fim da classe consulta

?>