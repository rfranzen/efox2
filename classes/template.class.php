<?php

  //Caminho para o arquivo Smarty.class.php
  require(DIRETORIO . 'libs/Smarty.class.php');

  class Tema extends Smarty {
	function Tema() {
	  // Construtor da Classe. Estes automaticamente sao definidos a cada nova instancia.
	  $this->Smarty();
	  $this->template_dir = DIRETORIO . TEMA;
	  $this->compile_dir = DIRETORIO . 'cache/';
	  $this->config_dir = DIRETORIO . 'config/';
	  $this->cache_dir = DIRETORIO . 'cache/';
	  $this->caching = false;
	  $this->force_compile = false;
	  $this->assign('app_name','eFox 2.0');
	}
  }

?>