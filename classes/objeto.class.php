<?php
/*
 * Classe de funções para objetos
 * - especialidades
 * - atividades
 * - cidades
 */


class objeto {

	private $bd;
	private $qry;
	public $codigo_pessoa;
	public $codigo;
	public $categoria;
	public $nome;
	public $tipo;
	public $rLobo;
	public $rEscot;
	public $rSen;
	public $rPio;
	public $comum;
	public $especial;
	public $area;
	public $cod_responsavel;
	public $cod_assistente;
	public $dataini;
	public $datafim;
	public $total;
	public $descricao;

	public $erros = array();

	public function __construct($conexao) {
		$this->bd = $conexao;
		$this->qry = new Consulta($conexao);
		$this->codigo = 0;
		$this->codigo_pessoa = 0;
	}

    public function incluir($array_campos, &$erro) {
        /*
         * Grava no BD os itens passados no array
        */
		$conexao = new Bd("mysql");
		$conexao->conectar(BANCO,SERVIDOR,USUARIO,SENHA);
		$objeto = new objeto($conexao);


		$sql = "INSERT INTO objeto(";

		$tamanho = count($array_campos);
		$y = $tamanho -1;
		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			if(($x != $y) && ($x < $y))
				$sql .= $campo . ", ";
			if($x == $y)
				$sql .= $campo;
			$x++;
		}
		$sql .= ") ";
		$sql .= "VALUES (";

		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			if(($x != $y) && ($x < $y))
				$sql .= "'" . $this->{$campo} . "', ";
			if($x == $y)
				$sql .= "'" . $this->{$campo} . "'";
			$x++;
		}
		$sql .= ")";

		$this->qry->executa($sql);

		if(!$this->qry->res) {
			$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
			return false;
		}
		else{
			$erro = "<span class=\"aviso\">Oba!.. Registro atualizado com sucesso!</span>";
			return true;
		}
    }

	public function alterar($array_campos, &$erro) {
		$sql = "UPDATE objeto SET ";
		$tamanho = count($array_campos);
		$y = $tamanho -1;
		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			if(($x != $y) && ($x < $y))
				$sql .= $campo . "='" . $this->{$campo} . "', ";
			if($x == $y)
				$sql .= $campo . "='" . $this->{$campo} . "'";
			$x++;
		}
		$sql .= " WHERE codigo ='" . $this->codigo ."'";
		$this->qry->executa($sql);

		if(!$this->qry->res) {
			$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
			return false;
		} else {
			$erro = "<span class=\"aviso\">Oba!.. Registro atualizado com sucesso!</span>";
			return  true;
		}
	}

	public function busca() {
		/*
		 * Busca os campos fornecidos no array
		*/

		$sql = "SELECT * FROM objeto WHERE codigo =" . $this->codigo;
		$this->qry->executa($sql);
		if(!$this->qry->res)
			return false;
		if ($this->qry->nrw == 1) {
			$this->codigo = $this->qry->data['codigo'];
			$this->categoria = $this->qry->data['categoria'];
			$this->nome = $this->qry->data['nome'];
			$this->tipo = $this->qry->data['tipo'];
			$this->rLobo = $this->qry->data['rLobo'];
			$this->rEscot = $this->qry->data['rEscot'];
			$this->rSen = $this->qry->data['rSen'];
			$this->rPio = $this->qry->data['rPio'];
			$this->comum = $this->qry->data['comum'];
			$this->especial = $this->qry->data['especial'];
			$this->area = $this->qry->data['area'];
			$this->cod_responsavel = $this->qry->data['cod_responsavel'];
			$this->cod_assistente = $this->qry->data['cod_assistente'];
			$this->dataini = $this->qry->data['dataini'];
			$this->datafim = $this->qry->data['datafim'];
			$this->total = $this->qry->data['total'];
			$this->descricao = $this->qry->data['descricao'];

			return true;
		} else {
			return false;
		}
	}

	public function apaga($codigo) {
		/*
		 * Apaga o objeto com o codigo fornecido
		*/
	}

	public function pega_nome($codigo) {
		$sql = "SELECT nome FROM objeto WHERE codigo = " . $codigo;
		$this->qry->executa($sql);

		return $this->qry->data['nome'];
	}

	public function listar($categoria, $tipo, $ordem) {
		/*
		 * Lista todos objetos relacionados a categoria informada
		*/

		$sql = "SELECT codigo, nome FROM objeto WHERE categoria = '" . $categoria . "'";
		$this->qry->executa($sql);
		$x = 1;
		if($this->qry->nrw <=0  or !$this->qry->res) {
			$array_retorno = null;
		}
		while($x <= $this->qry->nrw) {
			$array_retorno[$x]['codigo'] = $this->qry->data['codigo'];
			$array_retorno[$x]['nome'] = $this->qry->data['nome'];
			$x++;
			$this->qry->proximo();
		}
		$array_retorno['tamanho'] = $x;
		return $array_retorno;
	}

}

?>