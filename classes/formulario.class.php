<?php

class formulario {
	public $codigo;
//--------------------------Pessoa
	public $ueb;
		private $ueb_tamanho;
		private $ueb_desc;
	public $ativo;
		private $ativo_desc;
	public $login;
		private $login_tamanho;
		private $login_desc;
	public $email;
		private $email_tamanho;
		private $email_desc;
	public $nivel;
		private $nivel_desc;
	public $senha;
		private $senha_tamanho;
		private $senha_desc;
	public $senha_nova;
		private $senha_nova_tamanho;
		private $senha_nova_desc;
	public $nome;
		private $nome_tamanho;
		private $nome_desc;
	public $dataueb;
		private $dataueb_tamanho;
		private $dataueb_desc;
	public $url_foto;
		private $url_foto_desc;
	public $nascimento;
		private $nascimento_desc;
	public $cidade;
		private $cidade_tamanho;
		private $cidade_desc;
	public $endereco;
		private $endereco_tamanho;
		private $endereco_desc;
	public $numero;
		private $numero_tamanho;
		private $numero_desc;
	public $bairro;
		private $bairro_tamanho;
		private $bairro_desc;
	public $fone;
		private $fone_tamanho;
		private $fone_desc;
	public $naturalidade;
		private $naturalidade_tamanho;
		private $naturalidade_desc;
	public $estado_civil;
		private $estado_civil_tamanho;
		private $estado_civil_desc;
	public $religiao;
		private $religiao_tamanho;
		private $religiao_desc;
	public $serie;
		private $serie_tamanho;
		private $serie_desc;
	public $turno;
		private $turno_tamanho;
		private $turno_desc;
	public $instituicao;
		private $instituicao_tamanho;
		private $instituicao_desc;
	public $nome_pai;
		private $nome_pai_tamanho;
		private $nome_pai_desc;
	public $profissao_pai;
		private $profissao_pai_tamanho;
		private $profissao_pai_desc;
	public $fone_pai;
		private $fone_pai_tamanho;
		private $fone_pai_desc;
	public $nome_mae;
		private $nome_mae_tamanho;
		private $nome_mae_desc;
	public $profissao_mae;
		private $profissao_mae_tamanho;
		private $profissao_mae_desc;
	public $fone_mae;
		private $fone_mae_tamanho;
		private $fone_mae_desc;
//--------------------------Objeto
	public $tipo;
		private $tipo_tamanho;
		private $tipo_desc;
	public $rLobo;
		private $rLobo_desc;
	public $rEscot;
		private $rEscot_desc;
	public $rSen;
		private $rSen_desc;
	public $rPio;
		private $rPio_desc;
	public $area;
		private $area_tamanho;
		private $area_desc;
	public $cod_responsavel;
		private $cod_responsavel_tamanho;
		private $cod_responsavel_desc;
	public $cod_assistente;
		private $cod_assistente_tamanho;
		private $cod_assistente_desc;
	public $participante;
		private $participante_tamanho;
		private $participante_desc;
	public $dataini;
		private $dataini_desc;
	public $datafim;
		private $datafim_desc;
	public $total;
		private $total_tamanho;
		private $total_desc;
	public $descricao;
		private $descricao_desc;
	public $ramo;
		private $ramo_desc;
	public $ramo_registro;
	public $ramo_desligamento;
	public $grupo;
		private $grupo_tamanho;
		private $grupo_desc;
	public $especialidade;
		private $especialidade_desc;
	public $data1;
	public $itens1;
		private $itens1_desc;
	public $data2;
	public $itens2;
		private $itens2_desc;
	public $data3;
	public $itens3;
		private $itens3_desc;

	public $erros = array();
	public $array_retorno;

	public function __construct() {
		$this->codigo 	= null;
		$this->ueb 		= null;
		$this->senha 	= null;
		$this->senha_nova;
		$this->login 	= null;

		$this->ueb_tamanho 			= 10;
		$this->ueb_desc 			= "C&oacute;d. UEB";
		$this->login_tamanho 		= 30;
		$this->login_desc 			= "Login";
		$this->email_tamanho 		= 50;
		$this->email_desc 			= "Email";
		$this->senha_tamanho 		= 100;
		$this->senha_desc 			= "Senha";
		$this->senha_nova_tamanho 	= 100;
		$this->senha_nova_desc 		= "Nova Senha";
		$this->nome_tamanho 		= 100;
		$this->nome_desc 			= "Nome";
		$this->dataueb_tamanho 		= 4;
		$this->dataueb_desc 		= "Data de Inscri&ccedil;&atilde;o na UEB";
		$this->cidade_tamanho 		= 50;
		$this->cidade_desc 			= "Cidade";
		$this->endereco_tamanho 	= 50;
		$this->endereco_desc 		= "Endere&ccedil;o";
		$this->numero_tamanho 		= 4;
		$this->numero_desc 			= "N&uacute;mero";
		$this->bairro_tamanho 		= 50;
		$this->bairro_desc 			= "Bairro";
		$this->fone_tamanho 		= 8;
		$this->fone_desc 			= "Fone";
		$this->naturalidade_tamanho = 20;
		$this->naturalidade_desc 	= "Naturalidade";
		$this->estado_civil_tamanho = 10;
		$this->estado_civil_desc 	= "Estado Civil";
		$this->religiao_tamanho 	= 20;
		$this->religiao_desc 		= "Religi&atilde;o";
		$this->serie_tamanho 		= 4;
		$this->serie_desc 			= "S&eacute;rie";
		$this->turno_tamanho 		= 5;
		$this->turno_desc 			= "Turno";
		$this->instituicao_tamanho 	= 50;
		$this->instituicao_desc 	= "Institui&ccedil;&atilde;o";
		$this->nome_pai_tamanho 	= 50;
		$this->nome_pai_desc 		= "Nome do Pai";
		$this->profissao_pai_tamanho = 30;
		$this->profissao_pai_desc 	= "Profiss&atilde;o Pai";
		$this->fone_pai_tamanho 	= 8;
		$this->fone_pai_desc 		= "Fone Pai";
		$this->nome_mae_tamanho 	= 50;
		$this->nome_mae_desc 		= "Nome da M&atilde;e";
		$this->profissao_mae_tamanho = 30;
		$this->profissao_mae_desc 	= "Profiss&atilde;o M&atilde;e";
		$this->fone_mae_tamanho 	= 8;
		$this->fone_mae_desc 		= "Fone M&atilde;e";

		$this->ativo_desc 			= "Ativo / Inativo";
		$this->nascimento_desc 		= "Nascimento";
		$this->nivel_desc 			= "N&iacute;vel";
		$this->url_foto_desc 		= "Foto";

		$this->tipo_tamanho 		= 50;
		$this->tipo_desc 			= "Tipo";
		$this->rLobo_desc 			= "Lobo";
		$this->rEscot_desc 			= "Escoteiro";
		$this->rSen_desc 			= "S&ecirc;nior";
		$this->rPio_desc 			= "Pioneiro";
		$this->area_tamanho 		= 50;
		$this->area_desc 			= "&Aacute;rea";
		$this->cod_responsavel_tamanho = 10;
		$this->cod_responsavel_desc = "Respons&aacute;vel";
		$this->cod_assistente_tamanho = 10;
		$this->cod_assistente_desc 	= "Assistente";
		$this->participante_tamanho = 10;
		$this->participante_desc 	= "Participantes";
		$this->dataini_desc 		= "In&iacute;cio";
		$this->datafim_desc 		= "Fim";
		$this->total_tamanho 		= 4;
		$this->total_desc 			= "Total";
		$this->descricao_desc 		= "Descri&ccedil;&atilde;o";
		$this->ramo_desc			= "Ramo";
		$this->grupo_desc			= "Grupo Escoteiro";
		$this->especialidade_desc	= "Especialidade";
		$this->itens1_desc			= "Itens de N&iacute;vel 1";
		$this->itens2_desc			= "Itens de N&iacute;vel 2";
		$this->itens3_desc			= "Itens de N&iacute;vel 3";


		$this->erros = null;
		$this->nivel= null;
		$this->array_retorno = array();
	}

	public function cria($nome, $acao, $array_campos, $funcao, $obj, &$erro) {
		/*
		 * $nome - nome do formulario
		 * $acao - parametro do action
		 * $array_campos - campos que serao exibidos no form
		 * $funcao - se eh edicao, novo, etc
		 * &$erro - retorno dos possiveis erros
		*/
		$conexao = new Bd("mysql");
		$conexao->conectar(BANCO,SERVIDOR,USUARIO,SENHA);
		$objeto = new objeto($conexao);
		$usuario_objeto = new usuario_objeto($conexao);
		$usuario = new usuario($conexao);
		$efox = new efox();

		$retorno = "<form id=\"" . $nome . "\" name=\"" . $nome . "\" method=\"post\" action=\"" . $acao . "\" enctype=\"multipart/form-data\">\n";

		if($funcao != "edita")
			$retorno .= "<a href=\"modulos/mod_busca.php?objeto=" . $obj . "\" target=\"_blank\" onClick=\"window.open(this.href, this.target, \"width=400,height=500\"); return false;\"><spam class=\"pesquisa\">Pesquisar</spam></a>\n";

		$tamanho = count($array_campos);
		$y = $tamanho -1;
		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			switch ($campo) {
// Area ---------------------------------------------------
				case "area":
					if($obj == "atividade")
						$retorno .= "<p><input type=\"text\" id=\"" . $campo . "\" name=\"" . $campo . "\" size=\"20\" maxlength=\"" . $this->{$campo . "_tamanho"} . "\" value=\"" . $this->{$campo} . "\"> <label for=\"" . $campo . "\">Local</label></p>\n";
					if($obj == "cidade")
						$retorno .= "<p><input type=\"text\" id=\"" . $campo . "\" name=\"" . $campo . "\" size=\"20\" maxlength=\"" . $this->{$campo . "_tamanho"} . "\" value=\"" . $this->{$campo} . "\"> <label for=\"" . $campo . "\">CEP</label></p>\n";
					if($obj == "especialidade") {
						$lista = $objeto->listar("especialidade_tipo");
						$retorno .= "<p><select name=\"$campo\" id=\"$campo\">\n";
						for($i=1; $i<$lista['tamanho']; $i++) {
							if ($lista[$i]['codigo'] == $this->area) {
								$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\" selected=\"selected\">" . $lista[$i]['nome'] . "</option>\n";
							} else {
								$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\">" . $lista[$i]['nome'] . "</option>\n";
							}
						}
						$retorno .= "</select> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
					}
				break;
// Ativo ---------------------------------------------------
				case "ativo":
					if($this->{$campo} == 1)
						$ativo = "checked";
					else
						$ativo = null;
					$retorno .= "<p><input type=\"checkbox\" id=\"" . $campo . "\" name=\"" . $campo . "\" value=\"1\"" . $ativo . "> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
				break;
// Cidade ---------------------------------------------------
				case "cidade":
					if($funcao == "novo") {
						$retorno .= "<p><input type=\"text\" id=\"" . $campo . "\" name=\"" . $campo . "\" size=\"20\" maxlength=\"" . $this->{$campo . "_tamanho"} . "\" value=\"" . $this->{$campo} . "\"> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
					} else {
						$lista = $objeto->listar("cidade");
						$retorno .= "<p><select name=\"$campo\" id=\"$campo\">\n";
						for($i=1; $i<$lista['tamanho']; $i++) {
							if ($lista[$i]['codigo'] == $this->cidade) {
								$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\" selected=\"selected\">" . $lista[$i]['nome'] . "</option>\n";
							} else {
								$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\">" . $lista[$i]['nome'] . "</option>\n";
							}
						}
						$retorno .= "</select> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
					}
				break;
// Codigo ---------------------------------------------------
				case "codigo":
					if (($funcao == "edita") || ($funcao == "busca"))
						$retorno .= "<input type=\"hidden\" id=\"" . $campo . "\" name=\"" . $campo . "\" maxlength=\"" . $this->{$campo . "_tamanho"} . "\" value=\"" . $this->{$campo} . "\">\n";
				break;
// Cod Assistente ---------------------------------------------------
				case "cod_assistente":
					$lista = $usuario->listar("admin");
					$retorno .= "<p class=\"box\"><select name=\"" . $campo . "[]\" id=\"" . $campo . "\" size=\"5\" multiple>\n";

					$assistentes = explode(",", $this->{$campo});

					for($i=1; $i<=$lista['tamanho']; $i++) {
						if (in_array($lista[$i]['codigo'], $assistentes)) {
							$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\" selected=\"selected\">" . $lista[$i]['nome'] . "</option>\n";
						} else {
							$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\">" . $lista[$i]['nome'] . "</option>\n";
						}
					}
					$retorno .= "</select> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
				break;
// Cod Responsavel ---------------------------------------------------
				case "cod_responsavel":
					if($this->cod_responsavel == null)
						$default = 0;
					else
						$default = $this->cod_responsavel;

					$lista = $usuario->listar("admin");
					$retorno .= "<p><select name=\"" . $campo . "\" id=\"" . $campo . "\">\n";
					$retorno .= "<option value=\"0\">Selecione</option>\n";
					for($i=1; $i<$lista['tamanho']; $i++) {
						if ($lista[$i]['codigo'] == $this->cod_responsavel) {
							$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\" selected=\"selected\">" . $lista[$i]['nome'] . "</option>\n";
						} else {
							$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\">" . $lista[$i]['nome'] . "</option>\n";
						}
					}
					$retorno .= "</select> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
				break;
// Data1 ---------------------------------------------------
				case "data1":
					$dia = substr($this->{$campo},8,2);
					$mes = substr($this->{$campo},5,2);
					$ano = substr($this->{$campo},0,4);
					if($dia == null)
						$dia = 1;
					if($mes == null)
						$mes = 1;
					if($ano == null)
						$ano = 2008;

					$retorno .= "<p>";
					$retorno .= $efox->monta_select_numerico("1dia", 1, 31, $dia) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("1mes", 1, 12, $mes) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("1ano", 1950, date('Y'), $ano) . "&nbsp;&nbsp;<label for=\"nivel1\">N&iacute;vel 1</label> (<label for=\"1dia\">Dia</label>/<label for=\"1mes\">M&ecirc;s</label>/<label for=\"1ano\">Ano</label>)</p>\n";
				break;
// Data2 ---------------------------------------------------
				case "data2":
					$dia = substr($this->{$campo},8,2);
					$mes = substr($this->{$campo},5,2);
					$ano = substr($this->{$campo},0,4);
					if($dia == null)
						$dia = 1;
					if($mes == null)
						$mes = 1;
					if($ano == null)
						$ano = 2008;

					$retorno .= "<p>";
					$retorno .= $efox->monta_select_numerico("2dia", 1, 31, $dia) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("2mes", 1, 12, $mes) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("2ano", 1950, date('Y'), $ano) . "&nbsp;&nbsp;<label for=\"nivel2\">N&iacute;vel 2</label> (<label for=\"2dia\">Dia</label>/<label for=\"2mes\">M&ecirc;s</label>/<label for=\"2ano\">Ano</label>)</p>\n";
				break;
// Data3 ---------------------------------------------------
				case "data3":
					$dia = substr($this->{$campo},8,2);
					$mes = substr($this->{$campo},5,2);
					$ano = substr($this->{$campo},0,4);
					if($dia == null)
						$dia = 1;
					if($mes == null)
						$mes = 1;
					if($ano == null)
						$ano = 2008;

					$retorno .= "<p>";
					$retorno .= $efox->monta_select_numerico("3dia", 1, 31, $dia) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("3mes", 1, 12, $mes) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("3ano", 1950, date('Y'), $ano) . "&nbsp;&nbsp;<label for=\"nivel3\">N&iacute;vel 2</label> (<label for=\"3dia\">Dia</label>/<label for=\"3mes\">M&ecirc;s</label>/<label for=\"3ano\">Ano</label>)</p>\n";
				break;
// Dataini ---------------------------------------------------
				case "dataini":
					$dia = substr($this->{$campo},8,2);
					$mes = substr($this->{$campo},5,2);
					$ano = substr($this->{$campo},0,4);
					if($dia == null)
						$dia = 1;
					if($mes == null)
						$mes = 1;
					if($ano == null)
						$ano = 2008;

					$retorno .= "<p>";
					$retorno .= $efox->monta_select_numerico("idia", 1, 31, $dia) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("imes", 1, 12, $mes) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("iano", 1950, date('Y'), $ano) . "&nbsp;&nbsp;<label for=\"inicio\">In&iacute;cio</label> (<label for=\"idia\">Dia</label>/<label for=\"imes\">M&ecirc;s</label>/<label for=\"iano\">Ano</label>)</p>\n";
				break;
// Datafim ---------------------------------------------------
				case "datafim":
					$dia = substr($this->{$campo},8,2);
					$mes = substr($this->{$campo},5,2);
					$ano = substr($this->{$campo},0,4);
					if($dia == null)
						$dia = 1;
					if($mes == null)
						$mes = 1;
					if($ano == null)
						$ano = 2008;

					$retorno .= "<p>";
					$retorno .= $efox->monta_select_numerico("fdia", 1, 31, $dia) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("fmes", 1, 12, $mes) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("fano", 1950, date('Y'), $ano) . "&nbsp;&nbsp;<label for=\"fim\">Fim</label> (<label for=\"fdia\">Dia</label>/<label for=\"fmes\">M&ecirc;s</label>/<label for=\"fano\">Ano</label>)</p>\n";
				break;
// Descricao ---------------------------------------------------
				case "descricao":
					$retorno .= "<p><textarea name=\"" . $campo . "\" id=\"" . $campo . "\" rows=\"10\" cols=\"65\">" . $this->{$campo} . "</textarea> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
				break;
// Especialidade ---------------------------------------------------
				case "especialidade":
					$lista = $objeto->listar("especialidade");
					if($funcao == "novo") {
						$retorno .= "<p><select name=\"$campo\" id=\"$campo\">\n";
						for($i=1; $i<$lista['tamanho']; $i++) {
							if ($lista[$i]['codigo'] == $this->especialidade) {
								$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\" selected=\"selected\">" . $lista[$i]['nome'] . "</option>\n";
							} else {
								$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\">" . $lista[$i]['nome'] . "</option>\n";
							}
						}
						$retorno .= "</select> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
					} else if($funcao == "edita") {
						$retorno .= "<p><strong>Especialidade:</strong> ";
						for($i=1; $i<$lista['tamanho']; $i++) {
							if ($lista[$i]['codigo'] == $this->especialidade)
								$retorno .= $lista[$i]['nome'];
						}
						$retorno .= "</p>\n";
					}
				break;
// Grupo ---------------------------------------------------
				case "grupo":
					if($funcao == "novo") {
						$retorno .= "<p><input type=\"text\" id=\"" . $campo . "\" name=\"" . $campo . "\" size=\"20\" maxlength=\"" . $this->{$campo . "_tamanho"} . "\" value=\"" . $this->{$campo} . "\"> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
					} else {
						$lista = $objeto->listar("grupo");
						$retorno .= "<p><select name=\"$campo\" id=\"$campo\">\n";
						for($i=1; $i<$lista['tamanho']; $i++) {
							if ($lista[$i]['codigo'] == $this->grupo) {
								$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\" selected=\"selected\">" . $lista[$i]['nome'] . "</option>\n";
							} else {
								$retorno .= "<option value=\"" . $lista[$i]['codigo'] . "\">" . $lista[$i]['nome'] . "</option>\n";
							}
						}
						$retorno .= "</select> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
					}
				break;
// Nascimento ---------------------------------------------------
				case "nascimento":
					$dia = substr($this->{$campo},8,2);
					$mes = substr($this->{$campo},5,2);
					$ano = substr($this->{$campo},0,4);
					if($dia == null)
						$dia = 1;
					if($mes == null)
						$mes = 1;
					if($ano == null)
						$ano = 2008;

					$retorno .= "<p>";
					$retorno .= $efox->monta_select_numerico("dia", 1, 31, $dia) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("mes", 1, 12, $mes) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("ano", 1950, 2008, $ano) . "&nbsp;&nbsp;<label for=\"nascimento\">Nascimento</label> (<label for=\"dia\">Dia</label>/<label for=\"mes\">M&ecirc;s</label>/<label for=\"ano\">Ano</label>)</p>\n";
				break;
// Nivel ---------------------------------------------------
				case "nivel":
					if($obj == "especialidade") {
						if($this->nivel == null)
							$default = 1;
						else
							$default = $this->nivel;

						$retorno .= $efox->monta_select_numerico($campo, 1, 3, $default) . "&nbsp;<label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label>\n";
					} else if($obj == "pessoa") {
						if($this->nivel == null)
							$default = 3;
						else
							$default = $this->nivel;

						$nivel_array = array("Selecione","Administrador", "Chefe", "Escoteiro");
						$retorno .= "<p>";
						$retorno .= $efox->monta_select_array($campo, $nivel_array, $default);
						$retorno .= " <label for=\"nivel\">N&iacute;vel</label></p>\n";
					}
				break;
// Participante ---------------------------------------------------
				case "participante":
					$lista = $usuario_objeto->atividade_usuario($this->codigo);
					$usuarios = $usuario->listar("user",1);

					for($i=1; $i<$lista['tamanho']; $i++) {
						$atividade[$i] = $lista[$i]["codigo"];
					}

					for($i=1; $i<$usuarios['tamanho']; $i++) {
						if(in_array($usuarios[$i]["codigo"], $atividade))
							$ativo = "checked";
						else
							$ativo = null;

							$retorno .= "<input type=\"checkbox\" id=\"" . $usuarios[$i]["codigo"] . "\" name=\"" . $usuarios[$i]["codigo"] . "\" value=\"1\"" . $ativo . "> <label for=\"" . $usuarios[$i]["codigo"] . "\">" . $usuarios[$i]["nome"] . "</label><br />\n";
					}

					$retorno .= "</p>\n";
				break;
// Ramo ---------------------------------------------------
				case "ramo":
					$lista = $objeto->listar("ramo");

					$retorno .= "<div class=\"box\"><h4>" . $this->{$campo . "_desc"} . "</h4>\n <optgroup label=\"" . $campo . "\">\n";

					for($i=1; $i<$lista['tamanho']; $i++) {
						if($lista[$i]["codigo"] == $this->ramo)
							if($funcao == "edita")
								$retorno .= $lista[$i]["nome"];
							else
								$retorno .= "<input type=\"radio\" name=\"" . $campo . "\" id=\"" . $lista[$i]["nome"] . "\" value=\"" . $lista[$i]["codigo"] . "\" checked />&nbsp;<label for=\"" . $lista[$i]["nome"] . "\">" . $lista[$i]["nome"] . "</label>  \n";
						else
							if($funcao == "edita")
								$retorno .= "";
							else
								$retorno .= "<input type=\"radio\" name=\"" . $campo . "\" id=\"" . $lista[$i]["nome"] . "\" value=\"" . $lista[$i]["codigo"] . "\" />&nbsp;<label for=\"" . $lista[$i]["nome"] . "\">" . $lista[$i]["nome"] . "</label>  \n";
					}
					$retorno .= "</optgroup>\n";
				break;
// Ramo Registro ---------------------------------------------------
				case "ramo_registro":
					$dia = substr($this->{$campo},8,2);
					$mes = substr($this->{$campo},5,2);
					$ano = substr($this->{$campo},0,4);
					if($dia == null)
						$dia = 1;
					if($mes == null)
						$mes = 1;
					if($ano == null)
						$ano = 2008;

					$retorno .= "<p>";
					$retorno .= $efox->monta_select_numerico("rdia", 1, 31, $dia) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("rmes", 1, 12, $mes) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("rano", 1980, 2008, $ano) . "&nbsp;&nbsp;<label for=\"registro\">Registro</label> (<label for=\"rdia\">Dia</label>/<label for=\"rmes\">M&ecirc;s</label>/<label for=\"rano\">Ano</label>)</p>\n";
				break;
// Ramo Desligamento ---------------------------------------------------
				case "ramo_desligamento":
					$dia = substr($this->{$campo},8,2);
					$mes = substr($this->{$campo},5,2);
					$ano = substr($this->{$campo},0,4);
					if($dia == null)
						$dia = 1;
					if($mes == null)
						$mes = 1;
					if($ano == null)
						$ano = 2008;

					$retorno .= "<p>";
					$retorno .= $efox->monta_select_numerico("ddia", 1, 31, $dia) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("dmes", 1, 12, $mes) . "&nbsp;";
					$retorno .= $efox->monta_select_numerico("dano", 1980, 2008, $ano) . "&nbsp;&nbsp;<label for=\"desligamento\">Desligamento</label> (<label for=\"ddia\">Dia</label>/<label for=\"dmes\">M&ecirc;s</label>/<label for=\"dano\">Ano</label>) \n";
					$retorno .= "</div>\n";
				break;
// rLobo ---------------------------------------------------
				case "rLobo":
					if($this->{$campo} == 1)
						$ativo = "checked";
					else
						$ativo = null;
					$retorno .= "<p class=\"box\"><input type=\"checkbox\" id=\"" . $campo . "\" name=\"" . $campo . "\" value=\"1\"" . $ativo . "> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label> \n";
				break;
// rEscot ---------------------------------------------------
				case "rEscot":
					if($this->{$campo} == 1)
						$ativo = "checked";
					else
						$ativo = null;
					$retorno .= "<input type=\"checkbox\" id=\"" . $campo . "\" name=\"" . $campo . "\" value=\"1\"" . $ativo . "> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label> \n";
				break;
// rSen ---------------------------------------------------
				case "rSen":
					if($this->{$campo} == 1)
						$ativo = "checked";
					else
						$ativo = null;
					$retorno .= "<input type=\"checkbox\" id=\"" . $campo . "\" name=\"" . $campo . "\" value=\"1\"" . $ativo . "> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label> \n";
				break;
// rPio ---------------------------------------------------
				case "rPio":
					if($this->{$campo} == 1)
						$ativo = "checked";
					else
						$ativo = null;
					$retorno .= "<input type=\"checkbox\" id=\"" . $campo . "\" name=\"" . $campo . "\" value=\"1\"" . $ativo . "> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
				break;
// Senha ---------------------------------------------------
				case "senha":
					$retorno .= "<p><input type=\"password\" id=\"" . $campo . "\" name=\"" . $campo . "\" size=\"20\" maxlength=\"" . $this->{$campo . "_tamanho"} . "\" value=\"" . $this->{$campo} . "\"> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
				break;
// Senha Nova ---------------------------------------------------
				case "senha_nova":
					$retorno .= "<p><input type=\"password\" id=\"" . $campo . "\" name=\"" . $campo . "\" size=\"20\" maxlength=\"" . $this->{$campo . "_tamanho"} . "\" value=\"" . $this->{$campo} . "\"> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
				break;
// Tipo ---------------------------------------------------
				case "tipo":
					if($obj == "atividade") {
						$array = array("Selecione","Acampamento","Bivaque","Jornada","Reuni&atilde;o");
						$retorno .= "<p>" . $efox->monta_select_array($campo,$array,$this->{$campo}) . " <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
					}
				break;
// URL Foto ---------------------------------------------------
				case "url_foto":
					$retorno .= "url_foto";
				break;
				default:
					$retorno .= "<p><input type=\"text\" id=\"" . $campo . "\" name=\"" . $campo . "\" size=\"20\" maxlength=\"" . $this->{$campo . "_tamanho"} . "\" value=\"" . $this->{$campo} . "\"> <label for=\"" . $campo . "\">" . $this->{$campo . "_desc"} . "</label></p>\n";
			}
			$x++;
		}

		if($funcao == "busca")
			$retorno .= "<p><input name=\"submit\" type=\"submit\" id=\"submit\" value=\"Gerar Formulario\" /></p>\n";
		else
			$retorno .= "<p><input name=\"submit\" type=\"submit\" id=\"submit\" value=\"Gravar\" /></p>\n";
		$retorno .= "</form>\n";
		return $retorno;
	}

}

?>