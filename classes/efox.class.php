<?php
/*
 * Classe de funções do sistema efox2.0
 */

class efox {
	public $teste;

    public function endereco_atual() {
        /*
         * Retorna a URL atual
        */
        $server = $_SERVER['SERVER_NAME'];
        $endereco = $_SERVER ['REQUEST_URI'];
        $url = "http://" . $server . $endereco;
        return $url;
    }

	public function monta_select_numerico($nome, $inicio, $fim, $default) {
		/*
		 * Monta um select de numeros sequenciais entre o $inicio e $fim
		*/
		if(($nome == "") || ($inicio == "") || ($fim == "") || ($default == "")) {
			return false;
		}
		$select = "<select name=\"$nome\" id=\"$nome\">\n";
		for($i = $inicio; $i <= $fim; $i++) {
			if ($i == $default) {
				$select .= "\t<option value=\"". sprintf("%02d", $i) ."\" selected=\"selected\">".sprintf("%02d", $i)."</option>\n";
			} else {
				$select .= "\t<option value=\"". sprintf("%02d", $i) ."\">".sprintf("%02d", $i)."</option>\n";
			}
		}
		$select .= "</select>\n";
		return $select;
	}

	public function monta_select_array($nome, $array, $default) {
		/*
		 * Monta um select com os campos do array informado
		*/
		if($array == "") {
			return false;
		}
		$select = "<select name=\"$nome\" id=\"$nome\">\n";
		for($i = 0; $i < count($array); $i++) {
			if ($i == $default) {
				$select .= "<option value=\"$i\" selected=\"selected\">{$array[$i]}</option>\n";
			} else {
				$select .= "<option value=\"$i\">{$array[$i]}</option>\n";
			}
		}
		$select .= "</select>\n";
		return $select;
	}

	public function explode_config($arquivo, $parametro, $tipo=NULL) {
		/*
		 * Funcao utilizada para trazer os parametros do arquivo de configuracao
		*/
		if($tipo == NULL) {
			$retorno = explode($parametro, $arquivo);
			$retorno = explode("'", $retorno[1]);
			return $retorno[2];
		} else if ($tipo == "tema") {
			$retorno = explode($parametro, $arquivo);
			$retorno = explode("/", $retorno[1]);
			$retorno = explode("'", $retorno[1]);
			return $retorno[0];
		}
	}

}

?>