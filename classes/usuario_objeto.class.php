<?php
/*
 * Classe de funções para objetos
 * - especialidades
 * - atividades
 * - cidades
 */


class usuario_objeto {

	private $bd;
	private $qry;
	public $codigo_usuario;
	public $nome_usuario;
	public $codigo_objeto;
	public $nivel;
	public $itens1;
	public $data1;
	public $itens2;
	public $data2;
	public $itens3;
	public $data3;
	public $grupo;
	public $inscricao;
	public $desligamento;

	public $erros = array();

	public function __construct($conexao) {
		$this->bd = $conexao;
		$this->qry = new Consulta($conexao);
		$this->codigo_usuario = 0;
		$this->codigo_objeto = 0;
	}

    public function incluir($array_campos, &$erro) {
        /*
         * Grava no BD os itens passados no array
        */
		$conexao = new Bd("mysql");
		$conexao->conectar(BANCO,SERVIDOR,USUARIO,SENHA);
		$objeto = new objeto($conexao);

		$sql = "INSERT INTO usuario_objeto(";

		$tamanho = count($array_campos);
		$y = $tamanho -1;
		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			if(($x != $y) && ($x < $y))
				$sql .= $campo . ", ";
			if($x == $y)
				$sql .= $campo;
			$x++;
		}
		$sql .= ") ";
		$sql .= "VALUES(";

		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			if(($x != $y) && ($x < $y))
				$sql .= "'" . $this->{$campo} . "', ";
			if($x == $y)
				$sql .= "'" . $this->{$campo} . "'";
			$x++;
		}
		$sql .= ")";

		$this->qry->executa($sql);
		if(!$this->qry->res) {
			$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
			return false;
		}
		else{
			$erro = "<span class=\"aviso\">Oba!.. Registro atualizado com sucesso!</span>";
			return true;
		}
    }

	public function alterar($array_campos, $codigo_obj, $tipo, &$erro) {
		if($tipo == "atividade") {
			$sql = "DELETE FROM usuario_objeto WHERE codigo_objeto=" . $codigo_obj;
			$this->qry->executa($sql);
			if(!$this->qry->res) {
				$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
				return false;
			} else {
				$sql = "INSERT INTO usuario_objeto(codigo_usuario,codigo_objeto) VALUES ";

				$tamanho = count($array_campos);
				$y = $tamanho;
				$x=1;
				while($x <= $tamanho) {
					if(($x != $y) && ($x < $y))
						$sql .= "('" . $array_campos[$x] . "', '" . $codigo_obj . "'), ";
					if($x == $y)
						$sql .= "('" . $array_campos[$x] . "', '" . $codigo_obj . "')";
					$x++;
				}

				$this->qry->executa($sql);

				if(!$this->qry->res) {
					$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
					return false;
				} else {
					$erro = "<span class=\"aviso\">Oba!.. Registro atualizado com sucesso!</span>";
					return  true;
				}
			}
		} else {
			$sql = "UPDATE usuario_objeto SET ";
			$tamanho = count($array_campos);
			$y = $tamanho -1;
			$x=0;
			while($x < $tamanho) {
				$campo = $array_campos[$x];
				if(($x != $y) && ($x < $y))
					$sql .= $campo . "='" . $this->{$campo} . "', ";
				if($x == $y)
					$sql .= $campo . "='" . $this->{$campo} . "'";
				$x++;
			}
			$sql .= " WHERE codigo_usuario ='" . $this->codigo_usuario ."' AND codigo_objeto ='" . $this->codigo_objeto . "'";

			$this->qry->executa($sql);
			if(!$this->qry->res) {
				$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
				return false;
			} else {
				$erro = "<span class=\"aviso\">Oba!.. Registro atualizado com sucesso!</span>";
				return  true;
			}
		}
	}

	public function buscar_categoria($categoria) {
		/*
		 * retorna array com informacoes do usuario na categoria informada
		*/

		$sql = "SELECT usuario.nome,usuario_objeto.*
				FROM usuario_objeto,usuario
				WHERE usuario.codigo = " . $this->codigo_usuario . " AND codigo_usuario = " . $this->codigo_usuario . "
				AND codigo_objeto IN (SELECT codigo FROM objeto WHERE categoria = '" . $categoria . "')";
		$this->qry->executa($sql);

		$x = 1;
		if($this->qry->nrw <=0  or !$this->qry->res) {
			$array_retorno = null;
		}
		while($x <= $this->qry->nrw) {
			$array_retorno[$x]['codigo_usuario'] 	= $this->qry->data['codigo_usuario'];
			$array_retorno[$x]['nome_usuario'] 		= $this->qry->data['nome'];
			$array_retorno[$x]['codigo_objeto'] 	= $this->qry->data['codigo_objeto'];
			$array_retorno[$x]['nivel'] 			= $this->qry->data['nivel'];
			$array_retorno[$x]['itens1'] 			= $this->qry->data['iten1'];
			$array_retorno[$x]['data1'] 			= $this->qry->data['data1'];
			$array_retorno[$x]['itens2'] 			= $this->qry->data['iten2'];
			$array_retorno[$x]['data2'] 			= $this->qry->data['data2'];
			$array_retorno[$x]['itens3'] 			= $this->qry->data['iten3'];
			$array_retorno[$x]['data3'] 			= $this->qry->data['data3'];
			$array_retorno[$x]['grupo'] 			= $this->qry->data['grupo'];
			$array_retorno[$x]['inscricao'] 		= $this->qry->data['inscricao'];
			$array_retorno[$x]['desligamento'] 		= $this->qry->data['desligamento'];
			$x++;
			$this->qry->proximo();
		}
		$array_retorno['tamanho'] = $x;
		return $array_retorno;
	}

	public function buscaUsuarioObjeto($codigo_usuario,$codigo_objeto) {
		$sql = "SELECT * FROM usuario_objeto WHERE codigo_usuario = " . $codigo_usuario . " AND codigo_objeto = " . $codigo_objeto;
		$this->qry->executa($sql);

		if(!$this->qry->res)
			return false;
		if ($this->qry->nrw == 1) {
			$this->codigo_usuario 	= $this->qry->data['codigo_usuario'];
			$this->codigo_objeto 	= $this->qry->data['codigo_objeto'];
			$this->nivel 			= $this->qry->data['nivel'];
			$this->itens1 			= $this->qry->data['itens1'];
			$this->data1 			= $this->qry->data['data1'];
			$this->itens2 			= $this->qry->data['itens2'];
			$this->data2 			= $this->qry->data['data2'];
			$this->itens3 			= $this->qry->data['itens3'];
			$this->data3 			= $this->qry->data['data3'];
			$this->grupo 			= $this->qry->data['grupo'];
			$this->inscricao 		= $this->qry->data['inscricao'];
			$this->desligamento 	= $this->qry->data['desligamento'];

			return true;
		} else {
			return false;
		}
	}

	public function apaga($codigo_usuario,$codigo_objeto) {
		/*
		 * Apaga o objeto com o codigo fornecido
		*/
	}

	public function listaUsuarioCategoria($codigo_usuario,$categoria) {
		/*
		 * Lista todos objetos relacionados ao codigo e categoria informado
		*/

		$sql = "SELECT objeto.* FROM objeto WHERE objeto.codigo IN (SELECT codigo_objeto FROM usuario_objeto WHERE codigo_usuario = " . $codigo_usuario . ") AND objeto.categoria = '" . $categoria . "'";
		$this->qry->executa($sql);
		$x = 1;
		if($this->qry->nrw <=0  or !$this->qry->res) {
			$array_retorno = null;
		}
		while($x <= $this->qry->nrw) {
			$array_retorno[$x]['codigo'] = $this->qry->data['codigo'];
			$array_retorno[$x]['categoria'] = $this->qry->data['categoria'];
			$array_retorno[$x]['nome'] = $this->qry->data['nome'];
			$array_retorno[$x]['tipo'] = $this->qry->data['tipo'];
			$array_retorno[$x]['rLobo'] = $this->qry->data['rLobo'];
			$array_retorno[$x]['rEscot'] = $this->qry->data['rEscot'];
			$array_retorno[$x]['rSen'] = $this->qry->data['rSen'];
			$array_retorno[$x]['rPio'] = $this->qry->data['rPio'];
			$array_retorno[$x]['area'] = $this->qry->data['area'];
			$array_retorno[$x]['cod_responsavel'] = $this->qry->data['cod_responsavel'];
			$array_retorno[$x]['cod_assistente'] = $this->qry->data['cod_assistente'];
			$array_retorno[$x]['dataini'] = $this->qry->data['dataini'];
			$array_retorno[$x]['datafim'] = $this->qry->data['datafim'];
			$array_retorno[$x]['total'] = $this->qry->data['total'];
			$array_retorno[$x]['descricao'] = $this->qry->data['descricao'];
			$x++;
			$this->qry->proximo();
		}
		$array_retorno['tamanho'] = $x;
		return $array_retorno;
	}

	public function atividade_usuario($codigo) {
		/*
		 * Retorna os usuarios das quais estao relacionados ao codigo da atividade informada
		*/

		$sql = "SELECT usuario.nome,usuario.codigo FROM usuario WHERE usuario.codigo IN (SELECT codigo_usuario FROM usuario_objeto ";
		if($codigo != null) {
			$sql .= "WHERE codigo_objeto = " . $codigo . ")";
		} else {
			$sql .= ")";
		}
		$sql .= " AND ativo = 1";

		$this->qry->executa($sql);
		$x=1;
		while($x <= $this->qry->nrw) {
			$array_retorno[$x]['codigo'] = $this->qry->data['codigo'];
			$array_retorno[$x]['nome'] = $this->qry->data['nome'];
			$x++;
			$this->qry->proximo();
		}
		$array_retorno['tamanho'] = $x;
		return $array_retorno;
	}

}

?>