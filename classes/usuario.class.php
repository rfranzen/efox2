<?php

class Usuario {
	private $bd;
	private $qry;
	public $codigo;
	public $ueb;
	public $ativo;
	public $login;
	public $email;
	public $nivel;
	public $senha;
	public $nome;
	public $dataueb;
	public $url_foto;
	public $nascimento;
	public $cidade;
	public $endereco;
	public $numero;
	public $bairro;
	public $fone;
	public $naturalidade;
	public $estado_civil;
	public $religiao;
	public $serie;
	public $turno;
	public $instituicao;
	public $nome_pai;
	public $profissao_pai;
	public $fone_pai;
	public $nome_mae;
	public $profissao_mae;
	public $fone_mae;

	public $erros = array();
	public $array_retorno;
	public $senha_nova;

	public function __construct($conexao) {
		$this->bd = $conexao;
		$this->senha = 0;
		$this->qry = new Consulta($conexao);
		$this->codigo = 0;
		$this->ueb = 0;
		$this->ativo = 0;
		$this->senha = 0;
		$this->login = null;
		$this->erros = null;
		$this->nivel= null;

		$this->array_retorno = array();
		$this->senha_nova = null;
	}

	public function login($senha, $login, &$erro) {
		$this->login = $login;
		$busca = $this->buscar("l");
		if(!$busca) {
			$erro = "Usuario nao cadastrado ou senha invalida";
			return false;
		}
		elseif ($this->senha != crypt($senha,"em") ) {
			$erro = "Usuario nao cadastrado ou senha invalida";
			return false;
		}
		elseif ($this->ativo != 1) {
			$erro = "Usuario nao cadastrado ou senha invalida";
			return false;
		}

		return true;
	}

	public function logout() {
		session_unset($_SESSION['usuario']);
		session_destroy(); // destruicao das sessoes
		clearstatcache(); //limpeza da cache
		header("location:index.php");
	}

	public function incluir($array_campos, &$erro="") {
		if($this->buscar("l")) {
			$erro = "Usuario com login informado ja esta cadastrado";
			return false;
		}
		$this->senha =  crypt($this->senha,"em");

		$sql = "INSERT INTO usuario(";

		$tamanho = count($array_campos);
		$y = $tamanho -1;
		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			if(($x != $y) && ($x < $y))
				$sql .= $campo . ", ";
			if($x == $y)
				$sql .= $campo;
			$x++;
		}
		$sql .= ") ";
		$sql .= "VALUES(";

		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			if(($x != $y) && ($x < $y))
				if($this->{$campo} == null) {
					$sql .= "'null', ";
				} else
					$sql .= "'" . $this->{$campo} . "', ";
			if($x == $y)
				if($this->{$campo} == null) {
					$sql .= "'null'";
				} else
					$sql .= "'" . $this->{$campo} . "'";
			$x++;
		}
		$sql .= ")";
		$this->qry->executa($sql);
		if(!$this->qry->res) {
			$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
			return false;
		}
		else{
			$erro = "<span class=\"aviso\">Oba!.. Usuario cadastrado com sucesso!</span>";
			return true;
		}
	}

	public function buscar($codigo,$chave) {
		$sql = "SELECT * FROM usuario WHERE ";
		if($chave == "l")
			$sql .= "login='".$this->login."'";
		else
			if($codigo)
				$sql .= "codigo ='". $codigo."'";
			else
				$sql .= "codigo ='". $this->codigo."'";
			$this->qry->executa($sql);
		if(!$this->qry->res)
			return false;
		if ($this->qry->nrw == 1) {
			$this->codigo = $this->qry->data['codigo'];
			$this->ueb = $this->qry->data['ueb'];
			$this->ativo = $this->qry->data['ativo'];
			$this->login = $this->qry->data['login'];
			$this->email = $this->qry->data['email'];
			$this->nivel = $this->qry->data['nivel'];
			$this->senha = $this->qry->data['senha'];
			$this->nome = $this->qry->data['nome'];
			$this->dataueb = $this->qry->data['dataueb'];
			$this->url_foto = $this->qry->data['url_foto'];
			$this->nascimento = $this->qry->data['nascimento'];
			$this->cidade = $this->qry->data['cidade'];
			$this->endereco = $this->qry->data['endereco'];
			$this->numero = $this->qry->data['numero'];
			$this->bairro = $this->qry->data['bairro'];
			$this->fone = $this->qry->data['fone'];
			$this->naturalidade = $this->qry->data['naturalidade'];
			$this->estado_civil = $this->qry->data['estado_civil'];
			$this->religiao = $this->qry->data['religiao'];
			$this->serie = $this->qry->data['serie'];
			$this->turno = $this->qry->data['turno'];
			$this->instituicao = $this->qry->data['instituicao'];
			$this->nome_pai = $this->qry->data['nome_pai'];
			$this->profissao_pai = $this->qry->data['profissao_pai'];
			$this->fone_pai = $this->qry->data['fone_pai'];
			$this->nome_mae = $this->qry->data['nome_mae'];
			$this->profissao_mae = $this->qry->data['profissao_mae'];
			$this->fone_mae = $this->qry->data['fone_mae'];

			$this->array_retorno['codigo'] = $this->codigo;
			$this->array_retorno['ueb'] = $this->ueb;
			$this->array_retorno['ativo'] = $this->ativo;
			$this->array_retorno['login'] = $this->login;
			$this->array_retorno['email'] = $this->email;
			$this->array_retorno['nivel'] = $this->nivel;
			$this->array_retorno['senha'] = $this->senha;
			$this->array_retorno['nome'] = $this->nome;
			$this->array_retorno['dataueb'] = $this->dataueb;
			$this->array_retorno['url_foto'] = $this->url_foto;
			$this->array_retorno['nascimento'] = $this->nascimento;
			$this->array_retorno['cidade'] = $this->cidade;
			$this->array_retorno['endereco'] = $this->endereco;
			$this->array_retorno['numero'] = $this->numero;
			$this->array_retorno['bairro'] = $this->bairro;
			$this->array_retorno['fone'] = $this->fone;
			$this->array_retorno['naturalidade'] = $this->naturalidade;
			$this->array_retorno['estado_civil'] = $this->estado_civil;
			$this->array_retorno['religiao'] = $this->religiao;
			$this->array_retorno['serie'] = $this->serie;
			$this->array_retorno['turno'] = $this->turno;
			$this->array_retorno['instituicao'] = $this->instituicao;
			$this->array_retorno['nome_pai'] = $this->nome_pai;
			$this->array_retorno['profissao_pai'] = $this->profissao_pai;
			$this->array_retorno['fone_pai'] = $this->fone_pai;
			$this->array_retorno['nome_mae'] = $this->nome_mae;
			$this->array_retorno['profissao_mae'] = $this->profissao_mae;
			$this->array_retorno['fone_mae'] = $this->fone_mae;
			return $this->array_retorno;
		}
		else{
			return false;
		}
	}

	public function alterar($array_campos, &$erro="") {
		if($this->senha_nova != $this->senha)
			$this->senha = crypt($this->senha_nova,"em");

		$sql = "UPDATE usuario SET ";
		$tamanho = count($array_campos);
		$y = $tamanho -1;
		$x=0;
		while($x < $tamanho) {
			$campo = $array_campos[$x];
			if(($x != $y) && ($x < $y))
				$sql .= $campo . "='" . $this->{$campo} . "', ";
			if($x == $y)
				$sql .= $campo . "='" . $this->{$campo} . "'";
			$x++;
		}
		//$sql .= "login='".$this->login."', ";
		//if($this->senha_nova != $this->senha)
		//	$sql.= " senha='" . crypt($this->senha_nova,"em") . "', ";
		$sql .= " WHERE codigo ='" . $this->codigo ."'";
		$this->qry->executa($sql);
		if(!$this->qry->res) {
			$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
			return false;
		} else {
			$erro = "<span class=\"aviso\">Oba!.. Registro atualizado com sucesso!</span>";
			return  true;
		}
	}

	public function excluir(&$erro) {
		$sql1 = "DELETE FROM usuario_objeto WHERE codigo_usuario=" . $this->codigo;
		$sql2 = "DELETE FROM usuario WHERE codigo=" . $this->codigo;
		$this->qry->executa($sql1);
		if(!$this->qry->res) {
			$erro = "<span class=\"aviso\">Ops!.. Deu algum problema</span>";
			return false;
		}
		else {
			$this->qry->executa($sql2);
			if($this->qry->res) {
				$erro = "<span class=\"aviso\">Oba!.. Registro excluido com sucesso!</span>";
				return true;
			} else {
				$erro = "<span class=\"aviso\">Ops, erro.. as atividades desta pessoa foram excluidas, porem seu perfil ainda esta ativo devido a algum problema.</span>";
			}
		}
	}

	public function listar($nivel, $ativo, $ordem) {
		/*
		 * Nivel deve ser informado como "admin" ou "user"
		*/
		$sql = "SELECT codigo, nome FROM usuario";
		if(isset($nivel)) {
			if($nivel == "admin")
				$sql .= " WHERE nivel != '3'";
			if($nivel == "user")
				$sql .= " WHERE nivel = '3'";
			if($ativo == 1)
				$sql .= " AND ativo = '1'";
		} else {
			if($ativo == 1)
				$sql .= " WHERE ativo = '1'";
		}

		switch ($ordem) {
			case 'codigo':
				$sql .= " ORDER BY ueb";
			break;
			case 'nome':
				$sql .= " ORDER BY nome";
			break;
			case 'login':
				$sql .= " ORDER BY login";
			break;
			default:
				$sql .= " ORDER BY nome";
		}
		$this->qry->executa($sql);
		$x = 1;
		if($this->qry->nrw <=0  or !$this->qry->res) {
			$array_retorno = null;
		}
		while($x <= $this->qry->nrw) {
			$array_retorno[$x]['codigo'] = $this->qry->data['codigo'];
			$array_retorno[$x]['ueb'] = $this->qry->data['ueb'];
			$array_retorno[$x]['nome'] = $this->qry->data['nome'];
			$array_retorno[$x]['login'] = $this->qry->data['login'];
			$array_retorno[$x]['tipo'] = $this->qry->data['tipo'];
			$x++;
			$this->qry->proximo();
		}
		$array_retorno['tamanho'] = $x;
		return $array_retorno;
	}

	public function pega_nome($codigo) {
		$sql = "SELECT nome FROM usuario WHERE codigo = " . $codigo;
		$this->qry->executa($sql);

		return $this->qry->data['nome'];
	}

	public function situacao($status="") {
		if($status == 1)//ativar usr
			$sql = "UPDATE usuario SET ativo=1";
		if($status == 0)//desativar usr
			$sql = "UPDATE usuario SET ativo=0";
		$sql .= " WHERE codigo='" . $this->codigo . "'";
		$this->qry->executa($sql);
		if(!$this->qry->res)
			return false;
		else
			return true;
	}

	public function verifica($campo, $valor) {
		$sql = "SELECT " . $campo . " FROM usuario WHERE " . $campo . " = '" . $valor . "'";
		$this->qry->executa($sql);
		if($this->qry->res) {
			if ($this->qry->nrw == 0) {
				return false;
			}
			if ($this->qry->nrw == 1) {
				$this->$campo = $this->qry->data[$campo];
				return true;
			}
		} else {
			return false;
		}
	}

	public function avisar($destino, $assunto, $conteudo) {
		if(mail($destino, $assunto, nl2br($conteudo)))
			return true;
		else
		return false;
	}

	public function ultimos($quantidade) {
		if($quantidade == null)
			$quantidade = 10;

		$sql = "SELECT codigo,nome FROM usuario order by codigo DESC LIMIT " . $quantidade;
		$this->qry->executa($sql);
		$x = 1;
		if($this->qry->nrw <=0  or !$this->qry->res) {
			$array_retorno = null;
		}
		while($x <= $this->qry->nrw) {
			$array_retorno[$x]['codigo'] = $this->qry->data['codigo'];
			$array_retorno[$x]['nome'] = $this->qry->data['nome'];

			$x++;
			$this->qry->proximo();
		}
		$array_retorno['tamanho'] = $x;
		return $array_retorno;
	}

	public function aniversariantes($mes) {
		if($mes == null)
			$mes = date('m');

		$sql = "SELECT codigo,nome,nascimento FROM usuario WHERE MONTH(nascimento)='" . $mes . "' ORDER BY DAY(nascimento)";
		$this->qry->executa($sql);
		$x = 1;
		if($this->qry->nrw <=0  or !$this->qry->res) {
			$array_retorno = null;
		}
		while($x <= $this->qry->nrw) {
			$array_retorno[$x]['codigo'] = $this->qry->data['codigo'];
			$array_retorno[$x]['nome'] = $this->qry->data['nome'];

			$dia = substr($this->qry->data['nascimento'],8,2);
			$mes = substr($this->qry->data['nascimento'],5,2);
			$ano = substr($this->qry->data['nascimento'],0,4);

			$array_retorno[$x]['nascimento'] = $dia . "/" . $mes . "/" . $ano;

			$x++;
			$this->qry->proximo();
		}
		$array_retorno['tamanho'] = $x;
		return $array_retorno;
	}

}//classe usuario

?>