<?php

class Menu {
    private $bd;
    private $qry;
    public $menu_codigo;
	public $menu_titulo;
    public $menu_nivel;
    public $menu_pai;
    public $menu_nome;
    public $menu_posicao;
    public $menu_classe;
    public $menu_args;

    public $erros = array();
    public $array_retorno;

    public function __construct($conexao) {
        $this->qry = new Consulta($conexao);
        $this->menu_id = 0;
        $this->menu_nivel = 0;
        $this->menu_pai = 0;
        $this->menu_nome = null;
        $this->menu_posicao = 0;
        $this->menu_classe = null;
        $this->menu_args = null;
    }

    public function monta_menu($posicao, $nivel, $pai) {
        $this->menu_posicao = $posicao;
        $this->menu_nivel = $nivel;
        $this->menu_pai = $pai;
        $sql = "SELECT * FROM menu WHERE ";
        $sql .= "nivel = '" . $this->menu_nivel . "' ";
        $sql .= "AND posicao = '" . $this->menu_posicao . "' ";
        if ($this->menu_pai != 0)
        $sql .= "AND pai = '" . $this->menu_pai . "' ";
        $sql .= "ORDER BY nome";
        $this->qry->executa($sql);
        if(!$this->qry->res) {
            return false;
        }
        else{
            $this->array_retorno['tamanho'] = $this->qry->nrw;
            for($i=0; $i<$this->qry->nrw; $i++) {
                $this->array_retorno['nome' . $i] = mysql_result($this->qry->res, $i, 'menu.nome');
				$this->array_retorno['titulo' . $i] = mysql_result($this->qry->res, $i, 'menu.titulo');
                $this->array_retorno['codigo' . $i] = mysql_result($this->qry->res, $i, 'menu.codigo');
                $this->array_retorno['nivel' . $i] = mysql_result($this->qry->res, $i, 'menu.nivel');
                $this->array_retorno['pai' . $i] = mysql_result($this->qry->res, $i, 'menu.pai');
                $this->array_retorno['nome' . $i] = mysql_result($this->qry->res, $i, 'menu.nome');
                $this->array_retorno['posicao' . $i] = mysql_result($this->qry->res, $i, 'menu.posicao');
                $this->array_retorno['classe' . $i] = mysql_result($this->qry->res, $i, 'menu.classe');
                $this->array_retorno['args' . $i] = mysql_result($this->qry->res, $i, 'menu.args');
            }
            return $this->array_retorno;
        }
    }

	public function menu_titulo($pai,$args) {
		$this->menu_pai = $pai;
		$this->menu_args = $args;

		$sql = "SELECT titulo FROM menu WHERE args = '" . $this->menu_args . "' AND pai = (SELECT codigo FROM menu WHERE args = '" . $this->menu_pai . "' LIMIT 1)";
		$this->qry->executa($sql);
        if(!$this->qry->res) {
            return false;
        }
        else {
			$retorno = $this->qry->data['titulo'];
			return $retorno;
		}
	}

}

?>