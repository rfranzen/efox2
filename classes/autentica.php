<?php
	require_once DIRETORIO."/classes/bd.class.php";
	require_once DIRETORIO."/classes/usuario.class.php";
	require_once DIRETORIO."/classes/objeto.class.php";
	require_once DIRETORIO."/classes/usuario_objeto.class.php";

	$conexao = new Bd("mysql");
	$conexao->conectar(BANCO,SERVIDOR,USUARIO,SENHA);
	$usuario_corrente = new Usuario($conexao);
	$usuario = new Usuario($conexao);
	$objeto = new objeto($conexao);
	$usuario_objeto = new usuario_objeto($conexao);

    session_start();

    //Executa o logout do usuario
    if(!isset($_GET['action']))
        $_GET['action'] = "";

    if($_GET['action'] == 'logout')
        $usuario_corrente->logout();

    //usuario logado
    if (!empty($_SESSION['codigo'])){
        $usuario_corrente->login = $_SESSION['login'];
        $usuario_corrente->codigo = $_SESSION['codigo'];
        $usuario_corrente->nome = $_SESSION['nome'];
        $usuario_corrente->nivel = $_SESSION['nivel'];
        $usuario_corrente->buscar();
    //logout forcado
    //unset($_SESSION['cod_usuario'], $_SESSION['login_usr'], $_SESSION['nome_usuario']);
    }
    elseif (!empty($_POST['usuario_login']) AND !empty($_POST['senha_login'])) {
    if(!$usuario_corrente->login($_POST['senha_login'], $_POST['usuario_login'], $erro)){
        echo "<span class=\"erro\">$erro</span>";
        $usuario_corrente->login = 0;
    }
    else{
        $_SESSION['codigo'] = $usuario_corrente->codigo;
        $_SESSION['login'] = $usuario_corrente->login;
        $_SESSION['nome'] = $usuario_corrente->nome;
        $_SESSION['nivel'] = $usuario_corrente->nivel;
    }
}
?>