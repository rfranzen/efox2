<?php
/* Modulo de geracao de formularios e fichas
 * --------------------------------------------
*/

$efox = new efox();
$formularios = new formulario();

$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

$obj = $_GET['obj'];

$array = array("codigo","nome");

switch ($obj) {
/* Ficha 120
 ----------------------*/
	case "120":
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$usuario->codigo = $_GET["codigo"];
			$usuario->buscar();

			$corpo .= "<h2>Ficha 120</h2>\n";
			$corpo .= "<a href=\"#\" title=\"Imprimir\" onclick=\"window.print()\"><div class=\"imprimir\">Imprimir</div></a>\n";
			$corpo .= "<table>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td><span class=\"negrito\">Registro na UEB:</span>" . $usuario->ueb .  "</td>\n";
			$corpo .= "<td><span class=\"negrito\">Ano de Registro:</span>" . $usuario->dataueb . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr><td colspan=\"2\"><span class=\"negrito\">Nome:</span>" . $usuario->nome . "</td></tr>\n";
			$corpo .= "<tr><td colspan=\"2\"><span class=\"negrito\">Data de Nascimento:</span>" . date('d/m/Y', strtotime($usuario->nascimento)) . "</td></tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td><span class=\"negrito\">Endere&ccedil;o:</span>" . $usuario->endereco . "</td>\n";
			$corpo .= "<td><span class=\"negrito\">N&uacute;mero:</span>" . $usuario->numero . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td><span class=\"negrito\">Bairro:</span>" . $usuario->bairro . "</td>\n";
			$corpo .= "<td><span class=\"negrito\">CEP:</span>" . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td><span class=\"negrito\">Fone:</span>" . $usuario->fone . "</td>\n";
			$corpo .= "<td><span class=\"negrito\">Naturalidade:</span>" . $usuario->naturalidade . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td><span class=\"negrito\">Estado Civil:</span>" . $usuario->estado_civil . "</td>\n";
			$corpo .= "<td><span class=\"negrito\">Religi&atilde;o:</span>" . $usuario->religiao . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td><span class=\"negrito\">S&eacute;rie:</span>" . $usuario->serie . "</td>\n";
			$corpo .= "<td><span class=\"negrito\">Turno:</span>" . $usuario->turno . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td colspan=\"2\"><span class=\"negrito\">Institui&ccedil;&atilde;o:</span>" . $usuario->instituicao . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td colspan=\"2\"><span class=\"negrito\">Nome do Pai:</span>" . $usuario->nome_pai . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td><span class=\"negrito\">Profiss&atilde;o:</span>" . $usuario->profissao_pai . "</td>\n";
			$corpo .= "<td><span class=\"negrito\">Fone:</span>" . $usuario->fone_pai . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td colspan=\"2\"><span class=\"negrito\">Nome da M&atilde;e:</span>" . $usuario->nome_mae . "</td>\n";
			$corpo .= "</tr>\n";
			$corpo .= "<tr>\n";
			$corpo .= "<td><span class=\"negrito\">Profiss&atilde;o:</span>" . $usuario->profissao_mae . "</td>\n";
			$corpo .= "<td><span class=\"negrito\">Fone:</span>" . $usuario->fone_mae . "</td>\n";
			$corpo .= "</table>\n";


		/* Lista as atividades relativas a pessoa */
			$corpo .= "<hr class=\"linha\" />";
			$corpo .= "<h3>Lista de Atividades</h3>\n";
			$lista = $usuario_objeto->listaUsuarioCategoria($usuario->codigo, "atividade");
			$corpo .= "<table>\n";
			$corpo .= "<tr class=\"negrito\">\n";
			$corpo .= "<td>Atividade</td><td>Inicio</td><td>Final</td><td>Total de Dias</td>";
			$corpo .= "</tr>\n";

			for($x=1; $x<sizeof($lista); $x++) {
				$corpo .= "<tr>\n";
				$corpo .= "<td>" . $lista[$x]['nome'] . "</td>\n";
				$corpo .= "<td>" . date('d/m/Y', strtotime($lista[$x]['dataini'])) . "</td>\n";
				$corpo .= "<td>" . date('d/m/Y', strtotime($lista[$x]['datafim'])) . "</td>\n";
				$corpo .= "<td>" . $lista[$x]['total'] . "</td>\n";
				$corpo .= "</tr>\n";
			}
			$corpo .= "</table>\n";

		/* Lista as especialidades relativas a pessoa */
			$corpo .= "<hr class=\"linha\" />";
			$corpo .= "<h3>Lista de Especialidades</h3>\n";
			$corpo .= "<table>\n";
			$corpo .= "<tr class=\"negrito\">\n";
			$corpo .= "<td>Especialidade</td><td>Nivel</td><td>Data1</td><td>Itens1</td><td>Data2</td><td>Itens2</td><td>Data3</td><td>Itens3</td>";
			$corpo .= "</tr>\n";

			$lista = $usuario_objeto->listaUsuarioCategoria($usuario->codigo, "especialidade");
			for($x=1; $x<sizeof($lista); $x++) {
				$usuario_objeto->buscaUsuarioObjeto($usuario->codigo, $lista[$x]['codigo']);
				$corpo .= "<tr>";
				$corpo .= "<td>" . $lista[$x]['nome'] . "</td>\n";
				$corpo .= "<td>" . $usuario_objeto->nivel . "</td>\n";
				$corpo .= "<td>" . date('d/m/Y', strtotime($usuario_objeto->data1)) . "</td>\n";
				$corpo .= "<td>" . $usuario_objeto->itens1 . "</td>\n";
				$corpo .= "<td>" . date('d/m/Y', strtotime($usuario_objeto->data2)) . "</td>\n";
				$corpo .= "<td>" . $usuario_objeto->itens2 . "</td>\n";
				$corpo .= "<td>" . date('d/m/Y', strtotime($usuario_objeto->data3)) . "</td>\n";
				$corpo .= "<td>" . $usuario_objeto->itens3 . "</td>\n";
				$corpo .= "</tr>";
			}
			$corpo .= "</table>\n";

		} else {
			$usuario->codigo = $_GET["codigo"];
			$usuario->buscar();
			$formularios->codigo = $_GET["codigo"];
			$formularios->nome = $usuario->nome;

			$corpo .= "<div class=\"box\">";
			$corpo .= "<h2>Ficha 120</h2>\n";
			$corpo .= "<p>" . $formularios->cria("120", $efox->endereco_atual(), $array, "busca", "pessoa", $erro) . "</p>\n";
			$corpo .= "</div>";
		}
	break;
/* Formulario C
 ----------------------*/
	case "formc":
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$objeto->codigo = $_GET["codigo"];
			$objeto->busca();

			$corpo .= "<h2>Formul&aacute;rio C</h2>\n";
			$corpo .= "<a href=\"#\" title=\"Imprimir\" onclick=\"window.print()\"><div class=\"imprimir\">Imprimir</div></a>\n";

			switch ($objeto->tipo) {
				case 1:
					$tipo = "Acampamento";
				break;
				case 2:
					$tipo = "Bivaque";
				break;
				case 3:
					$tipo = "Jornada";
				break;
				case 4:
					$tipo = "Reuni&atilde;o";
				break;
			}
			$corpo .= "<span class=\"negrito\">Tipo: </span>" . $tipo . "\n";
			$corpo .= "<span class=\"negrito\">Local: </span>" . $objeto->area . "\n";
			$corpo .= "<hr />\n";
			$corpo .= "<span class=\"negrito\">Sa&iacute;da: </span>" . $objeto->dataini . "<br />\n";
			$corpo .= "<span class=\"negrito\">Chegada Prevista: </span>" . $objeto->datafim . "\n";
			$corpo .= "<hr />\n";
			$corpo .= "<span class=\"negrito\">Participantes: </span>\n";
			if($objeto->rLobo == 1) $corpo .= " Lobos, ";
			if($objeto->rEscot == 1) $corpo .= " Escoteiros, ";
			if($objeto->rSen == 1) $corpo .= " Seniors, ";
			if($objeto->rPio == 1) $corpo .= " Pioneiros ";
			$corpo .= "<hr />\n";

			$usuario->buscar($objeto->cod_responsavel);
			$corpo .= "<span class=\"negrito\">Respons&aacute;vel: </span>" . $usuario->nome . "\n";

			$assistentes = explode(",", $objeto->cod_assistente);
			$corpo .= "<span class=\"negrito\">Assistentes: </span>";
			for($x=0; $x<sizeof($assistentes); $x++) {
				$usuario->buscar($assistentes[$x]);
				$corpo .= $usuario->nome . ", ";
			}
			$corpo .= "<br />\n";

			$corpo .= "<span class=\"negrito\">Observa&ccedil;&otilde;es: </span>" . $objeto->descricao . "\n";
			$corpo .= "<hr />\n";
			$corpo .= "<span class=\"negrito\">Material Necess&aacute;rio</span>\n";
			$corpo .= "<hr />\n";
			$corpo .= "<span class=\"negrito\">Autoriza&ccedil;&atilde;o de Sa&iacute;da</span>\n";
			$corpo .= "<p>Texto aqui...</p>\n";
			$corpo .= "<p>Data, nome e assinatura...</p>\n";

		} else {
			$objeto->codigo = $_GET["codigo"];
			$objeto->busca();
			$formularios->codigo = $_GET["codigo"];
			$formularios->nome = $objeto->nome;

			$corpo .= "<div class=\"box\">";
			$corpo .= "<h2>Formul&aacute;rio C</h2>\n";
			$corpo .= "<p>" . $formularios->cria("formc", $efox->endereco_atual(), $array, "busca", "atividade", $erro) . "</p>\n";
			$corpo .= "</div>";
		}
	break;
/* Formulario D
 ----------------------*/
	case "formd":
		if($_SERVER["REQUEST_METHOD"] == "POST") {

		} else {
			$objeto->codigo = $_GET["codigo"];
			$objeto->busca();
			$formularios->codigo = $_GET["codigo"];
			$formularios->nome = $objeto->nome;

			$corpo .= "<div class=\"box\">";
			$corpo .= "<h2>Formul&aacute;rio D</h2>\n";
			$corpo .= "<p>" . $formularios->cria("formd", $efox->endereco_atual(), $array, "busca", "atividade", $erro) . "</p>\n";
			$corpo .= "</div>";
		}
	break;
/* Formulario E
 ----------------------*/
	case "forme":
		if($_SERVER["REQUEST_METHOD"] == "POST") {

		} else {
			$objeto->codigo = $_GET["codigo"];
			$objeto->busca();
			$formularios->codigo = $_GET["codigo"];
			$formularios->nome = $objeto->nome;

			$corpo .= "<div class=\"box\">";
			$corpo .= "<h2>Formul&aacute;rio E</h2>\n";
			$corpo .= "<p>" . $formularios->cria("forme", $efox->endereco_atual(), $array, "busca", "atividade", $erro) . "</p>\n";
			$corpo .= "</div>";
		}
	break;
/* Formulario G
 ----------------------*/
	case "formg":
		if($_SERVER["REQUEST_METHOD"] == "POST") {

		} else {
			$objeto->codigo = $_GET["codigo"];
			$objeto->busca();
			$formularios->codigo = $_GET["codigo"];
			$formularios->nome = $objeto->nome;

			$corpo .= "<div class=\"box\">";
			$corpo .= "<h2>Formul&aacute;rio G</h2>\n";
			$corpo .= "<p>" . $formularios->cria("formg", $efox->endereco_atual(), $array, "busca", "atividade", $erro) . "</p>\n";
			$corpo .= "</div>";
		}
	break;
/* Padrao
 ----------------------*/
	default:
		$corpo .= "<p><a href=\"" . $efox->endereco_atual() . "&amp;obj=120\">Ficha 120</a></p>\n";
		$corpo .= "<p><a href=\"" . $efox->endereco_atual() . "&amp;obj=formc\">Formul&aacute;rio C</a></p>\n";
		$corpo .= "<p><a href=\"" . $efox->endereco_atual() . "&amp;obj=formd\">Formul&aacute;rio D</a></p>\n";
		$corpo .= "<p><a href=\"" . $efox->endereco_atual() . "&amp;obj=forme\">Formul&aacute;rio E</a></p>\n";
		$corpo .= "<p><a href=\"" . $efox->endereco_atual() . "&amp;obj=formg\">Formul&aacute;rio G</a></p>\n";
}


$template->assign("corpo", $corpo);
$template->assign("titulo", $titulo);

?>