<?php
/* Modulo de opcoes de usuario
 * --------------------------------------
 *
*/

$efox = new efox();

$file = "config/config.php";
$config_file = file_get_contents($file);

$op = $_GET['op'];

if($usuario_corrente->nivel != 1) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {
    $titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
    $corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	switch ($op) {
		case "config":
			if($_SERVER["REQUEST_METHOD"] == "POST") {

				$servidor = $_POST["servidor"];
				$banco = $_POST["banco"];
				$usuario = $_POST["usuario"];
				$senha = $_POST["senha"];
				$diretorio = $_POST["diretorio"];
				$url = $_POST["url"];
				$tema = $_POST["template"];

				$localiza[0] = '/' . $efox->explode_config($config_file, "SERVIDOR") . '/';
				$localiza[1] = '/' . $efox->explode_config($config_file, "BANCO") . '/';
				$localiza[2] = '/' . $efox->explode_config($config_file, "USUARIO") . '/';
				$localiza[3] = '/' . $efox->explode_config($config_file, "SENHA") . '/';
				$localiza[4] = '/' . $efox->explode_config($config_file, "TEMA", "tema") . '/';
				$substitui[0] = $servidor;
				$substitui[1] = $banco;
				$substitui[2] = $usuario;
				$substitui[3] = $senha;
				$substitui[4] = $tema;
				$arquivo = preg_replace($localiza, $substitui, $config_file);

				//escreve no arquivo
				$fp = fopen($file, "w");
				fwrite($fp,$arquivo);
				fclose($fp);

				$corpo .= "As informa&ccedil;&otilde;es foram salvas corretamente.";

			} else {
				$corpo .= "<h2>Alterar configura&ccedil;&otilde;es do sistema</h2>\n";
				// Inicia a montagem do form
				$corpo .= "<form id=\"config\" name=\"config\" method=\"post\" action=\"". $efox->endereco_atual() . "\" enctype=\"multipart/form-data\">\n";

				$valor = $efox->explode_config($config_file, "SERVIDOR");
				$corpo .= "<p><input type=\"text\" id=\"servidor\" name=\"servidor\" value=\"" . $valor . "\" /> <label for=\"servidor\">Servidor de Banco de Dados</label></p>";

				$valor = $efox->explode_config($config_file, "BANCO");
				$corpo .= "<p><input type=\"text\" id=\"banco\" name=\"banco\" value=\"" . $valor . "\" /> <label for=\"banco\">Nome do Banco de Dados</label></p>";

				$valor = $efox->explode_config($config_file, "USUARIO");
				$corpo .= "<p><input type=\"text\" id=\"usuario\" name=\"usuario\" value=\"" . $valor . "\" /> <label for=\"usuario\">Usu&aacute;rio do Banco de Dados</label></p>";

				$valor = $efox->explode_config($config_file, "SENHA");
				$corpo .= "<p><input type=\"password\" id=\"senha\" name=\"senha\" value=\"" . $valor . "\" /> <label for=\"senha\">Senha do Banco de Dados</label></p>";

				$templates = "/home/www/efox2/templates";
				$abreDir=opendir($templates);
				$x = 1;
				while($arquivo=readdir($abreDir)) {
					if ($arquivo=='.' || $arquivo=='..')
						continue;
					$caminhoCompleto = "$templates/$arquivo";
					if (is_dir($caminhoCompleto)) {
						$template_array[$x] = $arquivo;
						$x++;
					}
				}
				// Monta o SELECT para selecao do template
				$corpo .= "<p>\n <select name=\"template\" id=\"template\">\n";
				for($x=1; $x <= sizeof($template_array); $x++) {
					if($template_array[$x] == $efox->explode_config($config_file, "TEMA", "tema")) {
						$corpo .= "<option value=\"" . $template_array[$x] . "\" selected=\"selected\">" . $template_array[$x] . "</option>\n";
					} else {
						$corpo .= "<option value=\"" . $template_array[$x] . "\">" . $template_array[$x] . "</option>\n";
					}
					$corpo .= $efox->explode_config($config_file, "TEMA", "tema");
				}
				// fim do select
				$corpo .= "</select>\n <label for=\"template\">Template</label></p>\n";

				$corpo .= "<p><input name=\"submit\" type=\"submit\" id=\"submit\" value=\"Gravar\" /></p>";
			// fim do form
				$corpo .= "</form>\n";
			}
			break;
		case "template":
			$templates = "/home/www/efox2/templates";
			$abreDir=opendir($templates);
			$x = 1;
			while($arquivo=readdir($abreDir)) {
				if ($arquivo=='.' || $arquivo=='..')
					continue;
				$caminhoCompleto = "$templates/$arquivo";
				if (is_dir($caminhoCompleto)) {
					$corpo .= $arquivo . "<br />";
					$x++;
				}
			}
			break;
		default:
			$corpo .= "<ul>\n";
			$corpo .= "<li><a href=\"". $efox->endereco_atual() . "&amp;op=config\">Configura&ccedil;&otilde;es de Sistema</a></li>\n";
			$corpo .= "<li><a href=\"". $efox->endereco_atual() . "&amp;op=template\">Editar Templates</a></li>\n";
			$corpo .= "</ul>\n";
	}
}

    $template->assign("corpo", $corpo);
    $template->assign("titulo", $titulo);

?>
