<?php
/* Modulo do usuario
 * ------------------
*/

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	$array = array("senha","senha_nova");

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$usuario->codigo = $usuario_corrente->codigo;
		$usuario->senha = $_POST["senha"];
		if ($_POST["senha_nova"] != null)
			$usuario->senha_nova = $_POST["senha_nova"];
		else
			$usuario->senha_nova = $_POST["senha"];

		$array = array("senha");
		$usuario->alterar($array, $erro);

		$corpo .= $erro;
	} else {
		$corpo .= "<h2>Digite a Nova Senha</h2>\n";
		$corpo .= "<p><strong>IMPORTANTE:</strong> Preencha apenas o campo \"Nova Senha\".</p>\n";

		$usuario->codigo = $usuario_corrente->codigo;
		$usuario->buscar();

		$formularios->codigo = $usuario->codigo;
		$formularios->senha = $usuario->senha;

		$corpo .= $formularios->cria("altera_senha", $efox->endereco_atual(), $array, "edita", null, $erro);
	}

	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);

?>