<?php
/* Modulo de menu - Retorna as caixas separadas por posicao
 * --------------------------------------
 *
*/
    require_once DIRETORIO."classes/menu.class.php";

/*
 * Menu Topo
*/
    $Mtopo = new Menu($conexao);
    $Mtopo->monta_menu(1, $usuario_corrente->nivel);
    $MenuTopo = "<ul>\n";
    for($i=0; $i<$Mtopo->array_retorno['tamanho']; $i++) {
        $MenuTopo .= "<li><a href=\"./?sessao=" . $Mtopo->array_retorno['args' . $i] . "\">" . $Mtopo->array_retorno['nome' . $i] . "</a></li>\n";

        if($sessao != NULL) { $pai[$Mtopo->array_retorno['args' . $i]] = $Mtopo->array_retorno['codigo' . $i]; }
    }
    $MenuTopo .= "</ul>\n";
    $MenuLogof = $usuario_corrente->nome . " | <a href=\"./?action=logout\">Sair</a>";

    $template->assign("menuTopo", $MenuTopo);
    $template->assign("menuLogof", $MenuLogof);

/*
 * Menu Direito
*/

    if($sessao != NULL) {
        $Mdir = new Menu($conexao);
        $Mdir->monta_menu(2, $usuario_corrente->nivel, $pai[$sessao]);
        $MenuDir = "<ul>\n";
        for($i=0; $i<$Mdir->array_retorno['tamanho']; $i++) {
            $MenuDir .= "<li><a href=\"./?sessao=" . $sessao . "&amp;menu=" . $Mdir->array_retorno['args' . $i] . "\">" . $Mdir->array_retorno['nome' . $i] . "</a></li>\n";
        }
        $MenuDir .= "</ul>\n";

        $template->assign("menuDir", $MenuDir);
    }

?>