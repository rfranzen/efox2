<?php
/* Modulo de opcoes de usuario
 * --------------------------------------
 * .listaUsuarios -> classe da listagem de usuarios
 * .novoUsuario -> exibe a barra "novo usuario" no topo da listagem
*/
if($usuario_corrente->nivel != 1) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	switch ($op) {
		case "novo":
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$ueb = $_POST["ueb"];
				$dataueb = $_POST["dataueb"];
				$nome = $_POST["nome"];
				$login = $_POST["login"];
				$senha = $_POST["senha"];
				$email = $_POST["email"];
				$nivel = $_POST["nivel"];

				$erro = NULL;

				if(!$ueb || !$dataueb || !$nome || !$login || !$senha || !$email || !$nivel) {
					$corpo .= "<spam class=\"erro\">Voc&ecirc; deve preencher todos os campos!</spam>";
					break;
				}
				else if ($usuario_corrente->verifica("ueb", $ueb))
					$erro .= "C&oacute;digo da UEB j&aacute; cadastrado<br />";
				else if ($usuario_corrente->verifica("nome", $nome))
					$erro .= "Nome j&aacute; cadastrado<br />";
				else if ($usuario_corrente->verifica("login", $login))
					$erro .= "Nome de usu&aacute;rio j&aacute; cadastrado<br />";
				else if ($usuario_corrente->verifica("email", $email))
					$erro .= "Email j&aacute; cadastrado<br />";
				if ($erro != NULL) {
					$corpo .= "<spam class=\"erro\">ERRRO: " . $erro . "</spam>";
					break;
				}
				else {
					$corpo .= "<h2>Gravar novo usu&aacute;rio</h2>\n";
					$usuario->ueb = $ueb;
					$usuario->dataueb = $dataueb;
					$usuario->nome = $nome;
					$usuario->login = $login;
					$usuario->senha = $senha;
					$usuario->email = $email;
					$usuario->nivel = $nivel;

					$array_campos = array("ueb", "dataueb", "nome", "login", "senha", "email", "nivel");
					$usuario->incluir($array_campos, $erro);

					$corpo .= $erro;
				}
			} else {
				$corpo .= "<h2>Novo Usu&aacute;rio</h2>\n";

				$campos_array = array("ueb", "dataueb", "nome", "login", "senha", "email", "nivel");
				$corpo .= $formularios->cria("usuario",$efox->endereco_atual(),$campos_array,$op,"pessoa",$erro);
			}
		break;
		case "exclui":
			$codigo = $_GET["codigo"];
			$nome = $_GET["nome"];

			$corpo .= "<h2>Excluir usu&aacute;rio</h2>\n";

		    if($_GET['action'] == 'sim') {
				$usuario->codigo = $codigo;
				if($usuario->excluir($erro))
					$corpo .= $erro;
				else
					$corpo .= $erro;
			} else {
				$corpo .= "<strong>Confirma Exclus&atilde;o do usu&aacute;rio " . $nome . "?</strong><br />\n";
				$corpo .= "<p><a href=\"" . $efox->endereco_atual() . "&amp;action=sim\"><span class=\"botao\">Sim</spam></a>";
				$corpo .= "<a href=\"javascript:history.go(-1)\"><span class=\"botao\">N&atilde;o! Quero voltar</spam></a></p>";
			}

		break;
		case "edita":
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$usuario->codigo = $_POST["codigo"];
				$usuario->ueb = $_POST["ueb"];
				$usuario->dataueb = $_POST["dataueb"];
				$usuario->nome = $_POST["nome"];
				$usuario->login = $_POST["login"];
				$usuario->senha = $_POST["senha"];
				$usuario->email = $_POST["email"];
				$usuario->nivel = $_POST["nivel"];
				if ($_POST["senha_nova"] != null)
					$usuario->senha_nova = $_POST["senha_nova"];
				else
					$usuario->senha_nova = $_POST["senha"];

				$campos_array = array("ueb", "dataueb", "nome", "login", "senha", "email", "nivel");
				$usuario->alterar($campos_array, $erro);

				$corpo .= $erro;
			} else {
				$corpo .= "<h2>Editar usu&aacute;rio</h2>\n";

				$usuario->codigo = $_GET["codigo"];
				$usuario->buscar();

				$formularios->codigo = $usuario->codigo;
				$formularios->ueb = $usuario->ueb;
				$formularios->dataueb = $usuario->dataueb;
				$formularios->nome = $usuario->nome;
				$formularios->login = $usuario->login;
				$formularios->senha = $usuario->senha;
				$formularios->email = $usuario->email;
				$formularios->nivel = $usuario->nivel;

				$campos_array = array("codigo", "ueb", "dataueb", "nome", "login", "senha", "senha_nova", "email", "nivel");
				$corpo .= $formularios->cria("atualiza_usuario",$efox->endereco_atual(),$campos_array,$op,"pessoa",$erro);
			}
		break;
		default:
			/* Novo Usuario */
			$corpo .= "<div class=\"novoUsuario\">\n";
			$corpo .= "<a href=\"" . $efox->endereco_atual() . "&amp;op=novo\">Novo Usu&aacute;rio</a>";
			$corpo .= "</div>\n";

			/* Listagem de usuarios */
			$lista = $usuario->listar();

			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"" . $efox->endereco_atual() . "&amp;op=edita&amp;codigo=" . $lista[$i]['codigo'] . "\">Editar</a> | <a href=\"" . $efox->endereco_atual() . "&amp;op=exclui&amp;codigo=" . $lista[$i]['codigo'] . "&amp;nome=" . $lista[$i]['nome'] . "\">Excluir</a>&nbsp;&nbsp;&nbsp;&nbsp;" . $lista[$i]['nome'] . "</li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
	}
}
	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);

?>