<?php
/* Modulo de cadastro
 * --------------------------------------
 * atividade, especialidade, pessoa
*/
if(($usuario_corrente->nivel != 1) && ($usuario_corrente->nivel != 2)) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	if($op == null) {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$objeto->categoria 	= "especialidade";
			$objeto->nome		= $_POST["nome"];
			$objeto->tipo		= $_POST["tipo"];
			$objeto->area		= $_POST["area"];
			$objeto->total		= $_POST["total"];
			$objeto->descricao	= $_POST["descricao"];

			if($_POST["codigo"] != null) {
				$objeto->codigo	= $_POST["codigo"];
				$campos_array = array("codigo","categoria","nome","area","total","descricao");
				$objeto->alterar($campos_array, $erro);
			} else {
				$campos_array = array("categoria","nome","area","total","descricao");
				$objeto->incluir($campos_array, $erro);
			}
			$corpo .= $erro;
		} else {
			if($_GET["sessao"] == "consulta") {
				$corpo .= "<h2>Consultar Especialidade</h2>\n";

			} else if($_GET["sessao"] == "cadastro") {
				$corpo .= "<h2>Cadastrar Especialidade</h2>\n";

				if($_GET['codigo'] != null) {
					$objeto->codigo = $_GET['codigo'];
					$objeto->busca();
					$formularios->codigo	= $objeto->codigo;
					$formularios->nome 		= $objeto->nome;
					$formularios->tipo 		= $objeto->tipo;
					$formularios->area 		= $objeto->area;
					$formularios->total 	= $objeto->total;
					$formularios->descricao = $objeto->descricao;

					$campos_array = array("codigo","nome","area","total","descricao");

					$corpo .= $formularios->cria("cadastra_especialidade",$efox->endereco_atual(),$campos_array,"edita",$_GET["menu"],$erro);
				} else {
					$campos_array = array("nome","area","total","descricao");
					$corpo .= $formularios->cria("cadastra_especialidade",$efox->endereco_atual(),$campos_array,null,$_GET["menu"],$erro);
				}
			}
		}
	}
}
	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);
?>