<?php
/* Modulo de gerenciamento de usuarios por ramo
 * --------------------------------------------
*/
if(($usuario_corrente->nivel != 1) && ($usuario_corrente->nivel != 2)) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	$array_campos = array("codigo","nome","especialidade","nivel","data1","itens1","data2","itens2","data3","itens3");
	$array_inserir = array("codigo_usuario","codigo_objeto","nivel","data1","itens1","data2","itens2","data3","itens3");
	$array_editar = array("nivel","data1","itens1","data2","itens2","data3","itens3");

	switch ($op) {
// Edita ------------------------------
		case "edita":
			$usuario_objeto->codigo_usuario = $_GET["codigo"];
			$usuario_objeto->codigo_objeto = $_GET["codigo_objeto"];


			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$usuario_objeto->nivel			= $_POST["nivel"];
				$usuario_objeto->data1 			= $_POST["1ano"]."-".$_POST["1mes"]."-".$_POST["1dia"];
				$usuario_objeto->itens1			= $_POST["itens1"];
				$usuario_objeto->data2 			= $_POST["2ano"]."-".$_POST["2mes"]."-".$_POST["2dia"];
				$usuario_objeto->itens2			= $_POST["itens2"];
				$usuario_objeto->data3 			= $_POST["3ano"]."-".$_POST["3mes"]."-".$_POST["3dia"];
				$usuario_objeto->itens3			= $_POST["itens3"];

				$usuario_objeto->alterar($array_editar,null, null, $erro);
				$corpo .= $erro;
			}

			$corpo .= "<h2>Editar especialidade da pessoa</h2>";

			$usuario_objeto->buscaUsuarioObjeto($_GET["codigo"], $_GET["codigo_objeto"]);

			$formularios->nome 				= $usuario->pega_nome($_GET["codigo"]);
			$formularios->codigo 			= $_GET["codigo"];
			$formularios->especialidade		= $_GET["codigo_objeto"];
			$formularios->nivel 			= $usuario_objeto->nivel;
			$formularios->data1 			= $usuario_objeto->data1;
			$formularios->itens1 			= $usuario_objeto->itens1;
			$formularios->data2 			= $usuario_objeto->data2;
			$formularios->itens2 			= $usuario_objeto->itens2;
			$formularios->data3 			= $usuario_objeto->data3;
			$formularios->itens3 			= $usuario_objeto->itens3;

			$corpo .= $formularios->cria("especialidade",$efox->endereco_atual(),$array_campos,"edita","especialidade",$erro);

		break;
// Lista ------------------------------
		case "lista":
			$usuario_objeto->codigo_usuario = $_GET["codigo"];

			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$usuario_objeto->codigo_objeto 	= $_POST["especialidade"];
				$usuario_objeto->nivel			= $_POST["nivel"];
				$usuario_objeto->data1 			= $_POST["1ano"]."-".$_POST["1mes"]."-".$_POST["1dia"];
				$usuario_objeto->itens1			= $_POST["itens1"];
				$usuario_objeto->data2 			= $_POST["2ano"]."-".$_POST["2mes"]."-".$_POST["2dia"];
				$usuario_objeto->itens2			= $_POST["itens2"];
				$usuario_objeto->data3 			= $_POST["3ano"]."-".$_POST["3mes"]."-".$_POST["3dia"];
				$usuario_objeto->itens3			= $_POST["itens3"];

				$usuario_objeto->incluir($array_inserir, $erro);
				$corpo .= $erro;
			}

			$lista = $usuario_objeto->buscar_categoria("especialidade");

			$corpo .= "<h2>Especialidades da pessoa</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"" . $efox->endereco_atual() . "&amp;op=edita&amp;codigo_objeto=" . $lista[$i]['codigo_objeto'] . "\"); self.close();\">Editar</a>  " . $objeto->pega_nome($lista[$i]['codigo_objeto']) . "</li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div><br /><br /><br />\n";

			$corpo .= "<h2>Nova especialidade para a pessoa</h2>";
			$formularios->nome = $usuario->pega_nome($_GET["codigo"]);
			$formularios->codigo = $_GET["codigo"];
			$corpo .= $formularios->cria("ramo",$efox->endereco_atual(),$array_campos,"novo","especialidade",$erro);

		break;
// Default ------------------------------
		default:
			$lista = $usuario->listar("user");

			$corpo .= "<h2>Selecione uma pessoa</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"" . $efox->endereco_atual() . "&amp;op=lista&amp;codigo=" . $lista[$i]['codigo'] . "\"); self.close();\">" . $lista[$i]['nome'] . "</a></li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
	}
}
	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);
?>