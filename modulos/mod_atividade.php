<?php
/* Modulo de cadastro
 * --------------------------------------
 * atividade, especialidade, pessoa
*/
if(($usuario_corrente->nivel != 1) && ($usuario_corrente->nivel != 2)) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	$campos_array = array("categoria","nome","tipo","rLobo","rEscot","rSen","rPio","area","cod_responsavel","cod_assistente","dataini","datafim","total","descricao");

	switch ($op) {
		case "edita":
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$objeto->categoria 			= "atividade";
				$objeto->codigo 			= $_POST["codigo"];
				$objeto->nome				= $_POST["nome"];
				$objeto->tipo				= $_POST["tipo"];
				if($_POST["rLobo"] == 1)
					$objeto->rLobo			= $_POST["rLobo"];
				if($_POST["rLobo"] == null)
					$objeto->rLobo			= 0;
				if($_POST["rEscot"] == 1)
					$objeto->rEscot			= $_POST["rEscot"];
				if($_POST["rEscot"] == null)
					$objeto->rEscot			= 0;
				if($_POST["rSen"] == 1)
					$objeto->rSen			= $_POST["rSen"];
				if($_POST["rSen"] == null)
					$objeto->rSen			= 0;
				if($_POST["rPio"] == 1)
					$objeto->rPio			= $_POST["rPio"];
				if($_POST["rPio"] == null)
					$objeto->rPio			= 0;

				$objeto->area				= $_POST["area"];
				$objeto->cod_responsavel	= $_POST["cod_responsavel"];

				$objeto->cod_assistente		= 0;
				foreach ($_POST["cod_assistente"] as $valor){
					$objeto->cod_assistente .= "," . $valor;
				}

				$objeto->dataini			= $_POST["iano"]."-".$_POST["imes"]."-".$_POST["idia"];
				$objeto->datafim			= $_POST["fano"]."-".$_POST["fmes"]."-".$_POST["fdia"];
				$objeto->total				= $_POST["total"];
				$objeto->descricao			= $_POST["descricao"];

				$campos_array = array("nome","tipo","rLobo","rEscot","rSen","rPio","area","cod_responsavel","cod_assistente","dataini","datafim","total","descricao");
				$objeto->alterar($campos_array, $erro);

				$corpo .= $erro;
			} else {
				$corpo .= "<h2>Editar Atividade</h2>\n";

				$objeto->codigo = $_GET["codigo"];
				$objeto->busca();

				$formularios->codigo 			= $objeto->codigo;
				$formularios->categoria 		= $objeto->categoria;
				$formularios->nome 				= $objeto->nome;
				$formularios->tipo 				= $objeto->tipo;
				$formularios->rLobo 			= $objeto->rLobo;
				$formularios->rEscot 			= $objeto->rEscot;
				$formularios->rSen 				= $objeto->rSen;
				$formularios->rPio 				= $objeto->rPio;
				$formularios->area 				= $objeto->area;
				$formularios->cod_responsavel 	= $objeto->cod_responsavel;
				$formularios->cod_assistente 	= $objeto->cod_assistente;
				$formularios->participante	 	= $objeto->participante;
				$formularios->dataini 			= $objeto->dataini;
				$formularios->datafim 			= $objeto->datafim;
				$formularios->total 			= $objeto->total;
				$formularios->descricao 		= $objeto->descricao;

				$campos_array = array("codigo","nome","tipo","rLobo","rEscot","rSen","rPio","area","cod_responsavel","cod_assistente","dataini","datafim","total","descricao");
				$corpo .= $formularios->cria("atualiza_atividade",$efox->endereco_atual(),$campos_array,$op,"atividade",$erro);
		}
		default:
			if($op == null) {
				if($_SERVER["REQUEST_METHOD"] == "POST") {
					$objeto->categoria 			= "atividade";
					$objeto->nome				= $_POST["nome"];
					$objeto->tipo				= $_POST["tipo"];

					if($_POST["rLobo"] == 1)
						$objeto->rLobo			= $_POST["rLobo"];
					if($_POST["rLobo"] == null)
						$objeto->rLobo			= 0;
					if($_POST["rEscot"] == 1)
						$objeto->rEscot			= $_POST["rEscot"];
					if($_POST["rEscot"] == null)
						$objeto->rEscot			= 0;
					if($_POST["rSen"] == 1)
						$objeto->rSen			= $_POST["rSen"];
					if($_POST["rSen"] == null)
						$objeto->rSen			= 0;
					if($_POST["rPio"] == 1)
						$objeto->rPio			= $_POST["rPio"];
					if($_POST["rPio"] == null)
						$objeto->rPio			= 0;

					$objeto->area				= $_POST["area"];
					$objeto->cod_responsavel	= $_POST["cod_responsavel"];

					$objeto->cod_assistente		= 0;
					foreach ($_POST["cod_assistente"] as $valor){
						$objeto->cod_assistente .= "," . $valor;
					}

					$objeto->dataini			= $_POST["iano"]."-".$_POST["imes"]."-".$_POST["idia"];
					$objeto->datafim			= $_POST["fano"]."-".$_POST["fmes"]."-".$_POST["fdia"];
					$objeto->total				= $_POST["total"];
					$objeto->descricao			= $_POST["descricao"];

					$objeto->incluir($campos_array, $erro);
					$corpo .= $erro;
				} else {
					if($_GET["sessao"] == "consulta") {
						$corpo .= "<h2>Consultar Atividade</h2>\n";

					} else if($_GET["sessao"] == "cadastro") {
						$corpo .= "<h2>Cadastrar Atividade</h2>\n";

						$campos_array = array("codigo","nome","tipo","rLobo","rEscot","rSen","rPio","area","cod_responsavel","cod_assistente","dataini","datafim","total","descricao");
						$corpo .= $formularios->cria("cadastra_atividade",$efox->endereco_atual(),$campos_array,null,$_GET["menu"],$erro);
					}
				}
			}
	}
}
	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);
?>