<?php
/* Modulo de gerenciamento de usuarios por ramo
 * --------------------------------------------
*/
if(($usuario_corrente->nivel != 1) && ($usuario_corrente->nivel != 2)) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	$array_campos = array("codigo","nome","ramo","ramo_registro","ramo_desligamento","grupo");
	$array_inserir = array("codigo_usuario","codigo_objeto","grupo","inscricao","desligamento");
	$array_editar = array("grupo","inscricao","desligamento");

	switch ($op) {
// Edita ------------------------------
		case "edita":
			$usuario_objeto->codigo_usuario = $_GET["codigo"];
			$usuario_objeto->codigo_objeto = $_GET["codigo_objeto"];

			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$usuario_objeto->inscricao 		= $_POST["rano"]."-".$_POST["rmes"]."-".$_POST["rdia"];
				$usuario_objeto->desligamento 	= $_POST["dano"]."-".$_POST["dmes"]."-".$_POST["ddia"];
				$usuario_objeto->grupo 			= $_POST["grupo"];

				$usuario_objeto->alterar($array_editar,null,null,$erro);
				$corpo .= $erro;
			}

			$corpo .= "<h2>Editar ramo da pessoa</h2>";

			$usuario_objeto->buscar($_GET["codigo"], $_GET["codigo_objeto"]);

			$formularios->nome 				= $usuario->pega_nome($_GET["codigo"]);
			$formularios->codigo 			= $_GET["codigo"];
			$formularios->ramo 				= $_GET["codigo_objeto"];
			$formularios->grupo 			= $usuario_objeto->grupo;
			$formularios->ramo_registro 	= $usuario_objeto->inscricao;
			$formularios->ramo_desligamento = $usuario_objeto->desligamento;

			$corpo .= $formularios->cria("ramo",$efox->endereco_atual(),$array_campos,"edita","ramo",$erro);
		break;
// Lista ------------------------------
		case "lista":
			$usuario_objeto->codigo_usuario = $_GET["codigo"];

			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$usuario_objeto->codigo_objeto 	= $_POST["ramo"];
				$usuario_objeto->inscricao 		= $_POST["rano"]."-".$_POST["rmes"]."-".$_POST["rdia"];
				$usuario_objeto->desligamento 	= $_POST["dano"]."-".$_POST["dmes"]."-".$_POST["ddia"];
				$usuario_objeto->grupo 			= $_POST["grupo"];

				$usuario_objeto->incluir($array_inserir, $erro);
				$corpo .= $erro;
			}

			$lista = $usuario_objeto->buscar_categoria("ramo");

			$corpo .= "<h2>Ramos da pessoa</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"" . $efox->endereco_atual() . "&amp;op=edita&amp;codigo_objeto=" . $lista[$i]['codigo_objeto'] . "\"); self.close();\">Editar</a>  " . $objeto->pega_nome($lista[$i]['codigo_objeto']) . "</li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div><br /><br /><br />\n";

			$corpo .= "<h2>Novo ramo para pessoa</h2>";
			$formularios->nome = $usuario->pega_nome($_GET["codigo"]);
			$formularios->codigo = $_GET["codigo"];
			$corpo .= $formularios->cria("ramo",$efox->endereco_atual(),$array_campos,"inserir","ramo",$erro);
		break;
// Default ------------------------------
		default:
			$lista = $usuario->listar(null,1);

			$corpo .= "<h2>Selecione uma pessoa</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"" . $efox->endereco_atual() . "&amp;op=lista&amp;codigo=" . $lista[$i]['codigo'] . "\"); self.close();\">" . $lista[$i]['nome'] . "</a></li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
	}
}
	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);
?>