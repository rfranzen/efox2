<?php
/* Modulo de cadastro
 * --------------------------------------
 * atividade, especialidade, pessoa
*/
if(($usuario_corrente->nivel != 1) && ($usuario_corrente->nivel != 2)) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	$campos_array = array("ueb","ativo","email","nome","dataueb","nascimento","cidade","endereco","numero","bairro","fone","naturalidade","estado_civil","religiao","serie","turno","instituicao","nome_pai","profissao_pai","fone_pai","nome_mae","profissao_mae","fone_mae");

	switch ($op) {
		case "edita":
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$usuario->codigo = $_POST["codigo"];
				$usuario->ativo = $_POST["ativo"];
				$usuario->ueb = $_POST["ueb"];
				$usuario->dataueb = $_POST["dataueb"];
				$usuario->nome = $_POST["nome"];
				$usuario->login = $_POST["login"];
				$usuario->senha = $_POST["senha"];
				$usuario->email = $_POST["email"];
				$usuario->nivel = $_POST["nivel"];
				$usuario->nascimento = $_POST["ano"]."-".$_POST["mes"]."-".$_POST["dia"];
				$usuario->cidade = $_POST["cidade"];
				$usuario->endereco = $_POST["endereco"];
				$usuario->numero = $_POST["numero"];
				$usuario->bairro = $_POST["bairro"];
				$usuario->fone = $_POST["fone"];
				$usuario->naturalidade = $_POST["naturalidade"];
				$usuario->estado_civil = $_POST["estado_civil"];
				$usuario->religiao = $_POST["religiao"];
				$usuario->serie = $_POST["serie"];
				$usuario->turno = $_POST["turno"];
				$usuario->instituicao = $_POST["instituicao"];
				$usuario->nome_pai = $_POST["nome_pai"];
				$usuario->profissao_pai = $_POST["profissao_pai"];
				$usuario->fone_pai = $_POST["fone_pai"];
				$usuario->nome_mae = $_POST["nome_mae"];
				$usuario->profissao_mae = $_POST["profissao_mae"];
				$usuario->fone_mae = $_POST["fone_mae"];

				$usuario->alterar($campos_array, $erro);

				$corpo .= $erro;
			} else {
				$corpo .= "<h2>Editar Pessoa</h2>\n";

				$usuario->codigo = $_GET["codigo"];
				$usuario->buscar();

				$formularios->codigo = $usuario->codigo;
				$formularios->ueb = $usuario->ueb;
				$formularios->ativo = $usuario->ativo;
				$formularios->email = $usuario->email;
				$formularios->nome = $usuario->nome;
				$formularios->dataueb = $usuario->dataueb;
				$formularios->nascimento = $usuario->nascimento;
				$formularios->cidade = $usuario->cidade;
				$formularios->endereco = $usuario->endereco;
				$formularios->numero = $usuario->numero;
				$formularios->bairro = $usuario->bairro;
				$formularios->fone = $usuario->fone;
				$formularios->naturalidade = $usuario->naturalidade;
				$formularios->estado_civil = $usuario->estado_civil;
				$formularios->religiao = $usuario->religiao;
				$formularios->serie = $usuario->serie;
				$formularios->turno = $usuario->turno;
				$formularios->instituicao = $usuario->instituicao;
				$formularios->nome_pai = $usuario->nome_pai;
				$formularios->profissao_pai = $usuario->profissao_pai;
				$formularios->fone_pai = $usuario->fone_pai;
				$formularios->nome_mae = $usuario->nome_mae;
				$formularios->profissao_mae = $usuario->profissao_mae;
				$formularios->fone_mae = $usuario->fone_mae;

				$campos_array = array("codigo","ueb","ativo","email","nome","dataueb","nascimento","cidade","endereco","numero","bairro","fone","naturalidade","estado_civil","religiao","serie","turno","instituicao","nome_pai","profissao_pai","fone_pai","nome_mae","profissao_mae","fone_mae");
				$corpo .= $formularios->cria("atualiza_pessoa",$efox->endereco_atual(),$campos_array,$op,"pessoa",$erro);
		}
		default:
			if($op == null) {
				if($_SERVER["REQUEST_METHOD"] == "POST") {
					$usuario->ativo = $_POST["ativo"];
					$usuario->ueb = $_POST["ueb"];
					$usuario->dataueb = $_POST["dataueb"];
					$usuario->nome = $_POST["nome"];
					$usuario->login = $_POST["login"];
					$usuario->senha = $_POST["senha"];
					$usuario->email = $_POST["email"];
					$usuario->nivel = $_POST["nivel"];
					$usuario->nascimento = $_POST["ano"]."-".$_POST["mes"]."-".$_POST["dia"];
					$usuario->cidade = $_POST["cidade"];
					$usuario->endereco = $_POST["endereco"];
					$usuario->numero = $_POST["numero"];
					$usuario->bairro = $_POST["bairro"];
					$usuario->fone = $_POST["fone"];
					$usuario->naturalidade = $_POST["naturalidade"];
					$usuario->estado_civil = $_POST["estado_civil"];
					$usuario->religiao = $_POST["religiao"];
					$usuario->serie = $_POST["serie"];
					$usuario->turno = $_POST["turno"];
					$usuario->instituicao = $_POST["instituicao"];
					$usuario->nome_pai = $_POST["nome_pai"];
					$usuario->profissao_pai = $_POST["profissao_pai"];
					$usuario->fone_pai = $_POST["fone_pai"];
					$usuario->nome_mae = $_POST["nome_mae"];
					$usuario->profissao_mae = $_POST["profissao_mae"];
					$usuario->fone_mae = $_POST["fone_mae"];

					$usuario->incluir($campos_array, $erro);

					$corpo .= $erro;
				} else {
					if($_GET["sessao"] == "consulta") {
						$corpo .= "<h2>Consultar Pessoa</h2>\n";

					} else if($_GET["sessao"] == "cadastro") {
						$corpo .= "<h2>Cadastrar Pessoa</h2>\n";

						$corpo .= $formularios->cria("cadastra_pessoa",$efox->endereco_atual(),$campos_array,null,$_GET["menu"],$erro);
					}
				}
			}
	}
}
	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);
?>