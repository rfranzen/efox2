<?php
/* Modulo de tela principal
 * --------------------------------------
 *
*/

/* Aniversariantes
------------------------*/
	$corpo = "<div class=\"principal_box\">\n";
	$corpo .= "<h2>Aniversariantes do M&ecirc;s</h2>\n";

	$lista = $usuario->aniversariantes();
	$corpo .= "<ul>\n";
	for($i=1; $i<$lista['tamanho']; $i++) {
		$corpo .= "<li>" . $lista[$i]['nascimento'] . " - " . $lista[$i]['nome'] . "</li>\n";
	}
	$corpo .= "</ul>\n";

	$corpo .= "</div>\n";

/* Avisos
------------------------*/
	$corpo .= "<div class=\"principal_box\">\n";
	$corpo .= "<h2>Avisos</h2>\n";
	$corpo .= "<p>\n";
	$corpo .= "Avisos aqui.<br /> Sem interface implementada";
	$corpo .= "</p>\n";
	$corpo .= "</div>\n";

/* Proximas Atividades
------------------------*/
	$corpo .= "<div class=\"principal_box\">\n";
	$corpo .= "<h2>Pr&oacute;ximas Atividades</h2>\n";
	$corpo .= "<p>\n";

	$lista = $objeto->listar("atividade");
	for($i=1; $i<$lista['tamanho']; $i++) {
		$corpo .= "<li>" . $lista[$i]['nome'] . "</li>\n";
	}

	$corpo .= "</p>\n";
	$corpo .= "</div>\n";

/* Ultimos Cadastros
------------------------*/
	$corpo .= "<div class=\"principal_box\">\n";
	$corpo .= "<h2>&Uacute;ltimos Cadastros</h2>\n";
	$lista = $usuario->ultimos();
	$corpo .= "<ul>\n";
	for($i=1; $i<$lista['tamanho']; $i++) {
		$corpo .= "<li>" . $lista[$i]['nome'] . "</li>\n";
	}
	$corpo .= "</ul>\n";

	$corpo .= "</div>\n";

/* Links Uteis
------------------------*/
	$corpo .= "<div class=\"principal_box\">\n";
	$corpo .= "<h2>Links &Uacute;teis</h2>\n";
	$corpo .= "<ul>\n";
	$corpo .= "<li><a href=\"#\">Site do Grupo</a></li>\n";
	$corpo .= "<li><a href=\"#\">Ueb Nacional</a></li>\n";
	$corpo .= "<li><a href=\"#\">Ueb RS</li></a>\n";
	$corpo .= "</ul>\n";
	$corpo .= "</div>\n";

	$template->assign("corpo", $corpo);

?>