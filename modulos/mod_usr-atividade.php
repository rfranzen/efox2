<?php
/* Modulo de gerenciamento de usuarios por ramo
 * --------------------------------------------
*/
if(($usuario_corrente->nivel != 1) && ($usuario_corrente->nivel != 2)) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	switch ($op) {
// Edita ------------------------------
		case "edita":
			$usuario_objeto->codigo_objeto = $_GET["codigo_objeto"];

			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$lista = $usuario->listar("user",1);

				$x=0;
				for($i=1; $i<$lista['tamanho']; $i++) {
					$codigo = $lista[$i]['codigo'];
					if($_POST[$codigo] != null) {
						$x++;
						$array[$x] .= $lista[$i]['codigo'];
					}
				}

				$usuario_objeto->alterar($array, $_GET["codigo"], "atividade", $erro);
				$corpo .= $erro;
			}

			$corpo .= "<h2>Editar participantes da atividade</h2>";

			$corpo .= "<p><strong>Atividade:</strong> " . $objeto->pega_nome($_GET["codigo"]) . "</p>";
			$corpo .= "<strong>Selecione os Participantes</strong>";

			$array = array("codigo","participante");
			$formularios->codigo = $_GET["codigo"];
			$corpo .= $formularios->cria("usuario-ramo",$efox->endereco_atual(),$array,"edita");

		break;
// Default ------------------------------
		default:
			$lista = $objeto->listar("atividade");

			$corpo .= "<h2>Selecione uma atividade</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"" . $efox->endereco_atual() . "&amp;op=edita&amp;codigo=" . $lista[$i]['codigo'] . "\"); self.close();\">" . $lista[$i]['nome'] . "</a></li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
	}
}
	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);
?>