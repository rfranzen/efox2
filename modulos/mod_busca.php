<?php

	require "../classes/bd.class.php";
	require "../classes/usuario.class.php";
	require "../classes/objeto.class.php";
	require "../config/config.php";
	require "../classes/efox.class.php";
	require "../classes/template.class.php";

	$conexao = new Bd("mysql");
	$conexao->conectar(BANCO,SERVIDOR,USUARIO,SENHA);

	$template = new Tema;
	$template->clear_all_cache();
	$efox = new efox();
	$usuario = new Usuario($conexao);
	$objeto = new objeto($conexao);


	$obj = $_GET["objeto"];
// Caso o login seja feito
	switch ($obj) {
		case "atividade":
			$lista = $objeto->listar($obj);

			$corpo .= "<h2>Lista de Atividades</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"#\" onClick=\"window.opener.location.replace(window.opener.location + '&amp;op=edita&amp;codigo=" . $lista[$i]['codigo'] . "'); self.close();\">" . $lista[$i]['nome'] . "</a></li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
		break;
		case "cidade":
			$lista = $objeto->listar("cidade",null);

			$corpo .= "<h2>Lista de Cidades</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"#\" onClick=\"window.opener.location.replace(window.opener.location + '&amp;codigo=" . $lista[$i]['codigo'] . "'); self.close();\">" . $lista[$i]['nome'] . "</a></li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
		break;
		case "especialidade":
			$lista = $objeto->listar("especialidade",null);

			$corpo .= "<h2>Lista de Especialidades</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"#\" onClick=\"window.opener.location.replace(window.opener.location + '&amp;codigo=" . $lista[$i]['codigo'] . "'); self.close();\">" . $lista[$i]['nome'] . "</a></li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
		break;
		case "grupo":
			$lista = $objeto->listar("grupo",null);

			$corpo .= "<h2>Lista de Grupos Escoteiros</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"#\" onClick=\"window.opener.location.replace(window.opener.location + '&amp;codigo=" . $lista[$i]['codigo'] . "'); self.close();\">" . $lista[$i]['nome'] . "</a></li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
		break;
		case "pessoa":
			$lista = $usuario->listar();

			$corpo .= "<h2>Lista de Usu&aacute;rios</h2>";
			$corpo .= "<div class=\"listaUsuarios\">\n";
			$corpo .= "<ul>\n";
			for($i=1; $i<$lista['tamanho']; $i++) {
				$corpo .= "<li><a href=\"#\" onClick=\"window.opener.location.replace(window.opener.location + '&amp;op=edita&amp;codigo=" . $lista[$i]['codigo'] . "'); self.close();\">" . $lista[$i]['nome'] . "</a></li>\n";
			}
			$corpo .= "</ul>\n";
			$corpo .= "</div>\n";
		break;
		default:
			$corpo .= "<a href=\"#\" onClick=\"window.opener.location.replace(window.opener.location'); self.close();\">Voltar</a>\n";
	}

	$template->assign("corpo", $corpo);
	$template->display('busca.php');

?>