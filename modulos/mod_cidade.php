<?php
/* Modulo de cadastro
 * --------------------------------------
 * atividade, especialidade, pessoa
*/
if($usuario_corrente->nivel != 1) {
	$corpo = "<span class=\"erro\">&Aacute;rea Restrita</span>";
}else {

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	$corpo .= "<h2>Cadastrar Cidade</h2>\n";

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$objeto->categoria = "cidade";
		$objeto->nome = $_POST["cidade"];
		$objeto->area = $_POST["area"];

		$campos_array = array("categoria","nome","area");

		$objeto->incluir($campos_array, $erro);

		$corpo .= $erro;
	}

	$campos_array = array("cidade","area");
	$corpo .= $formularios->cria("cadcidade",$efox->endereco_atual(),$campos_array,"novo","cidade",$erro);

	$corpo .= "<br /><br /><h2>Cidades Cadastradas</h2>\n";
	$lista = $objeto->listar("cidade");

	$corpo .= "<div class=\"listaUsuarios\">\n";
	$corpo .= "<ul>\n";
	for($i=1; $i<$lista['tamanho']; $i++) {
		$corpo .= "<li><a href=\"" . $efox->endereco_atual() . "&amp;codigo=" . $lista[$i]['codigo'] . "\">" . $lista[$i]['nome'] . "</a></li>\n";
	}
	$corpo .= "</ul>\n";
	$corpo .= "</div>\n";
}
	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);
?>