<?php
/* Modulo de cadastro
 * --------------------------------------
 * atividade, especialidade, pessoa
*/

	$efox = new efox();
	$formularios = new formulario();

	$titulo = $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]);
	$corpo = "<h1>" . $Mdir->menu_titulo($_GET["sessao"], $_GET["menu"]) . "</h1>\n";

	$campos_array = array("ueb","email","nome","dataueb","nascimento","cidade","endereco","numero","bairro","fone","naturalidade","estado_civil","religiao","serie","turno","instituicao","nome_pai","profissao_pai","fone_pai","nome_mae","profissao_mae","fone_mae");

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$usuario->codigo = $usuario_corrente->codigo;
		$usuario->ueb = $_POST["ueb"];
		$usuario->dataueb = $_POST["dataueb"];
		$usuario->nome = $_POST["nome"];
		$usuario->login = $_POST["login"];
		$usuario->senha = $_POST["senha"];
		$usuario->email = $_POST["email"];
		$usuario->nivel = $_POST["nivel"];
		$usuario->nascimento = $_POST["ano"]."-".$_POST["mes"]."-".$_POST["dia"];
		$usuario->cidade = $_POST["cidade"];
		$usuario->endereco = $_POST["endereco"];
		$usuario->numero = $_POST["numero"];
		$usuario->bairro = $_POST["bairro"];
		$usuario->fone = $_POST["fone"];
		$usuario->naturalidade = $_POST["naturalidade"];
		$usuario->estado_civil = $_POST["estado_civil"];
		$usuario->religiao = $_POST["religiao"];
		$usuario->serie = $_POST["serie"];
		$usuario->turno = $_POST["turno"];
		$usuario->instituicao = $_POST["instituicao"];
		$usuario->nome_pai = $_POST["nome_pai"];
		$usuario->profissao_pai = $_POST["profissao_pai"];
		$usuario->fone_pai = $_POST["fone_pai"];
		$usuario->nome_mae = $_POST["nome_mae"];
		$usuario->profissao_mae = $_POST["profissao_mae"];
		$usuario->fone_mae = $_POST["fone_mae"];

		$usuario->alterar($campos_array, $erro);
		$corpo .= $erro;
	} else {
		$corpo .= "<h2>Editar minhas informa&ccedil;&otilde;es</h2>\n";

		$usuario->codigo = $usuario_corrente->codigo;
		$usuario->buscar();

		$formularios->ueb = $usuario_corrente->ueb;
		$formularios->email = $usuario_corrente->email;
		$formularios->nome = $usuario_corrente->nome;
		$formularios->dataueb = $usuario_corrente->dataueb;
		$formularios->nascimento = $usuario_corrente->nascimento;
		$formularios->cidade = $usuario_corrente->cidade;
		$formularios->endereco = $usuario_corrente->endereco;
		$formularios->numero = $usuario_corrente->numero;
		$formularios->bairro = $usuario_corrente->bairro;
		$formularios->fone = $usuario_corrente->fone;
		$formularios->naturalidade = $usuario_corrente->naturalidade;
		$formularios->estado_civil = $usuario_corrente->estado_civil;
		$formularios->religiao = $usuario_corrente->religiao;
		$formularios->serie = $usuario_corrente->serie;
		$formularios->turno = $usuario_corrente->turno;
		$formularios->instituicao = $usuario_corrente->instituicao;
		$formularios->nome_pai = $usuario_corrente->nome_pai;
		$formularios->profissao_pai = $usuario_corrente->profissao_pai;
		$formularios->fone_pai = $usuario_corrente->fone_pai;
		$formularios->nome_mae = $usuario_corrente->nome_mae;
		$formularios->profissao_mae = $usuario_corrente->profissao_mae;
		$formularios->fone_mae = $usuario_corrente->fone_mae;

		$campos_array = array("ueb","email","nome","dataueb","nascimento","cidade","endereco","numero","bairro","fone","naturalidade","estado_civil","religiao","serie","turno","instituicao","nome_pai","profissao_pai","fone_pai","nome_mae","profissao_mae","fone_mae");
		$corpo .= $formularios->cria("atualiza_pessoa",$efox->endereco_atual(),$campos_array,"edita","pessoa",$erro);
	}

	$template->assign("corpo", $corpo);
	$template->assign("titulo", $titulo);
?>